#ifndef WDBUS_H
#define WDBUS_H

#include <dbus/dbus.h>
#include <sys/signalfd.h>
#include <sys/epoll.h>
#include <stdbool.h>

/* Defines */
#define WDBUS_DEFAULT_INTERFACE_NAME_OFFSET strlen("  <interface name=\"")

// Path name length is unlimited, but copy operations are limited to avoid
// overflows/excessive memory allocations from application errors
#define WDBUS_MAXIMUM_PATH_LENGTH DBUS_MAXIMUM_NAME_LENGTH

#ifdef __cplusplus
extern "C" {
#endif

/* Typedefs */
// Opaque types
typedef struct _WDBusContext WDBusContext;
typedef struct _WDBusObject WDBusObject;

// Interface types
typedef struct _WDBusMethod WDBusMethod;
typedef struct _WDBusInterface WDBusInterface;

// Message types
typedef struct _WDBusContainer WDBusContainer;
typedef struct _WDBusMessage WDBusMessage;
typedef enum _WDBusMessageType {
	WDBUS_MESSAGE_INVALID, WDBUS_MESSAGE_IN, WDBUS_MESSAGE_OUT, WDBusMessageTypeSize
} WDBusMessageType;

// Function pointer types
typedef DBusMessage *(*WDBusMethodCallback)(WDBusObject *object, DBusMessage *request);
typedef DBusMessage *(*WDBusPropertyCallback)(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request);
typedef void (*WDBusCallNotifyFunction)(WDBusMessage *msg, void *user_data);
typedef void (*WDBusFreeCallback)(void *data);

/* Structures */
// Interface structs
struct _WDBusMethod {
	char *name;
	WDBusMethodCallback callback;
};

struct _WDBusInterface {
	WDBusPropertyCallback get, get_all, set;
	int name_offset, name_len, xml_len, nmethod;
	char *xml;
	void *user_data;
	WDBusFreeCallback user_free;
	WDBusMethod methods[];
};

// Message structs
struct _WDBusContainer {
	DBusMessageIter iter;
	struct _WDBusContainer *next;
};

struct _WDBusMessage {
	WDBusMessageType type;
	DBusMessage *msg;
	WDBusContainer *top, bottom;
};

/* External definitions */
// Provided interfaces
extern WDBusInterface wdbus_introspectable_interface, wdbus_properties_interface;

/* Public Methods */
// Context Methods
WDBusContext *wdbus_context_create(int nevent);
void wdbus_context_set_user_data(WDBusContext *ctx, void *user_data, WDBusFreeCallback user_free);
void *wdbus_context_get_user_data(WDBusContext *ctx);
int wdbus_context_connect(WDBusContext *ctx, DBusBusType type, const char *name, unsigned int flags);
int wdbus_context_call(WDBusContext *ctx, WDBusMessage *msg, WDBusCallNotifyFunction callback, void *user_data, WDBusFreeCallback user_free, int timeout_ms);
int wdbus_context_call_sync(WDBusContext *ctx, WDBusMessage *msg, int timeout_ms);
int wdbus_context_register_object(WDBusContext *ctx, WDBusObject *obj);
int wdbus_context_unregister_object(WDBusContext *ctx, WDBusObject *obj);
int wdbus_context_setup(WDBusContext *ctx);
int wdbus_loop(WDBusContext *ctx, int timeout);
void wdbus_context_free(WDBusContext *ctx);
int wdbus_context_get_epfd(WDBusContext *ctx);

// Object Methods
WDBusObject *wdbus_object_create(const char *path);
void wdbus_object_set_user_data(WDBusObject *obj, void *user_data, WDBusFreeCallback user_free);
void *wdbus_object_get_user_data(WDBusObject *obj);
void *wdbus_object_get_context_data(WDBusObject *obj);
int wdbus_object_register_interface(WDBusObject *obj, WDBusInterface *interface);
int wdbus_object_prepare_introspection(WDBusObject *obj);
void wdbus_object_free(WDBusObject *obj);	// Registered objects are freed by wdbus_context_free
const char *wdbus_object_get_path(WDBusObject *obj);
int wdbus_object_emit_signal(WDBusObject *obj, WDBusMessage *signal);

// Interface methods
DBusMessage *wdbus_properties_default_get(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request);
DBusMessage *wdbus_properties_default_get_all(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request);
DBusMessage *wdbus_properties_default_set(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request);

// Message methods
int wdbus_message_init(WDBusMessage *wmsg, DBusMessage *msg, WDBusMessageType type);
int wdbus_message_get_arg_type(WDBusMessage *msg);
void wdbus_message_get_basic(WDBusMessage *msg, void *value);
bool wdbus_message_next_arg(WDBusMessage *msg);
int wdbus_message_append(WDBusMessage *msg, char type, const void *value);
int wdbus_message_recurse(WDBusMessage *msg, WDBusContainer *cont);
int wdbus_container_open(WDBusMessage *msg, WDBusContainer *cont, char type, const char *sign);
int wdbus_container_close(WDBusMessage *msg);
int wdbus_container_close_all(WDBusMessage *msg);
//int wdbus_container_abandon_all(WDBusMessage *msg);// TODO
int wdbus_dict_append(WDBusMessage *msg, char key_type, const void *key, char value_type, const void *value);
#define wdbus_array_open(msg, cont, type) wdbus_container_open(msg, cont, DBUS_TYPE_ARRAY, type)
#define wdbus_struct_open(msg, cont) wdbus_container_open(msg, cont, DBUS_TYPE_STRUCT, NULL)
#define wdbus_variant_open(msg, cont, type) wdbus_container_open(msg, cont, DBUS_TYPE_VARIANT, type)
#define wdbus_dict_open(msg, cont, key_type, value_type) wdbus_container_open(msg, cont, DBUS_TYPE_ARRAY, DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING key_type value_type DBUS_DICT_ENTRY_END_CHAR_AS_STRING)
#define wdbus_entry_open(msg, cont) wdbus_container_open(msg, cont, DBUS_TYPE_DICT_ENTRY, NULL)

#ifdef __cplusplus
}
#endif

#endif // WDBUS_H
