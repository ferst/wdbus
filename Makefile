export WDBUS_DIR:=$(shell pwd)

DEBUG_FLAGS := -Wall -O0 -g
export CFLAGS += -I$(WDBUS_DIR)/include $(shell pkg-config --cflags dbus-1) $(DEBUG_FLAGS)
export LINKS := $(shell pkg-config --libs dbus-1) -pthread

all: lib/libwdbus.so

example: example.c lib/libwdbus.so
	$(CC) $< -o $@ -L./lib -Wl,-rpath -Wl,./lib -lwdbus $(CFLAGS) $(LINKS)

examples:
	$(MAKE) -C examples

lib/libwdbus.so: src/wdbus.o src/wdbus-object.o src/wdbus-message.o
	[ -d ./lib ] || mkdir ./lib
	$(CC) -shared -fPIC $^ -o $@ $(CFLAGS)

src/%.o: src/%.c
	$(CC) $< -fPIC -c -o $@ $(CFLAGS)

.PHONY: clean examples

install:
	[ -d $(LIBDIR) ] || mkdir $(LIBDIR)
	[ -d $(INCLUDEDIR) ] || mkdir $(INCLUDEDIR)
	cp ./lib/libwdbus.so $(LIBDIR)
	cp ./include/wdbus.h $(INCLUDEDIR)

clean:
	-rm src/*.o example 2>/dev/null

distclean:
	$(MAKE) clean
	-rm lib/libwdbus.so 2>/dev/null

# Check https://gitlab.freedesktop.org/dbus/dbus/blob/master/README.valgrind
# about issues related to libdbus applications and Valgrind. It is recommended
# to compile debug version of libdbus apart from the system-wide one. The
# following rule serves as an example of how to run an example application with
# valgrind.

#DEBUG_DBUS_DIR =
#DEBUG_FLAGS += -L$(DEBUG_DBUS_DIR)/lib -Wl,-rpath -Wl,$(DEBUG_DBUS_DIR)/lib
#VALGRIND=valgrind
#VALGRIND_FLAGS = --leak-check=full --show-leak-kinds=all --track-fds=yes --track-origins=yes --trace-children=yes

#valgrind: example
#	$(VALGRIND) $(VALGRIND_FLAGS) env DBUS_MESSAGE_CACHE=0 LD_PRELOAD=$(DEBUG_DBUS_DIR)/lib/libdbus-1.so ./$<

