#ifndef WDBUS_PRIVATE_H
#define WDBUS_PRIVATE_H

#include "wdbus.h"
#include <stdio.h>

typedef enum _WDBusLogLevel {
	WDBUS_LOG_LEVEL_TRACE,
	WDBUS_LOG_LEVEL_DEBUG,
	WDBUS_LOG_LEVEL_INFO,
	WDBUS_LOG_LEVEL_WARN,
	WDBUS_LOG_LEVEL_ERROR,
	WDBusLogLevelSize
} WDBusLogLevel;
#define WDBUS_LOG(level, msg_fmt, ...) 
//fprintf(stderr, msg_fmt __VA_OPT__(,) __VA_ARGS__)

typedef struct _WDBusInterfaceList WDBusInterfaceList;

struct _WDBusObject {
	char *path, *xml;
	WDBusContext *ctx;
	WDBusInterfaceList *interfaces;
	int xml_dirty;
	WDBusFreeCallback user_free;
	void *user_data;
};

extern DBusObjectPathVTable wdbus_vt;

int wdbus_context_send_message(WDBusContext *ctx, WDBusMessage *msg);

#endif
