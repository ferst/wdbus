#include "wdbus.h"
#include "wdbus-private.h"

int wdbus_message_init(WDBusMessage *wmsg, DBusMessage *msg, WDBusMessageType type)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %p, %p, %s\n", __func__, wmsg, msg, type==WDBUS_MESSAGE_OUT?"WDBUS_MESSAGE_OUT":type==WDBUS_MESSAGE_IN?"WDBUS_MESSAGE_IN":type==WDBUS_MESSAGE_INVALID?"WDBUS_MESSAGE_INVALID":"Unknow WDBusMessageType");
	dbus_bool_t ret;

	wmsg->msg = msg;

	switch(type) {
		case WDBUS_MESSAGE_IN:
			ret = dbus_message_iter_init(msg, &wmsg->bottom.iter);
			break;
		case WDBUS_MESSAGE_OUT:
			// FIXME: check dbus_message_iter_init_append success
			// According to libdbus source, dbus-message.c:2502@v1.13.10,
			// dbus_message_iter_init_append never fails. Documentation says
			// that it can fail by OOM, but since it returns is void, its hard
			// to tell if it happen. Without the check, wdbus_message_init will
			// always return success for type == WDBUS_MESSAGE_OUT
			dbus_message_iter_init_append(msg, &wmsg->bottom.iter);
			ret = TRUE;
			break;
		case WDBUS_MESSAGE_INVALID:
		case WDBusMessageTypeSize:
		default:
			// TODO: log invalid type
			ret = FALSE;
	}

	if(ret == FALSE) return 1;

	wmsg->type = type;
	wmsg->bottom.next = NULL;
	wmsg->top = &wmsg->bottom;

	return 0;
}

int wdbus_message_get_arg_type(WDBusMessage *msg)
{
	return dbus_message_iter_get_arg_type(&msg->top->iter);
}

bool wdbus_message_next_arg(WDBusMessage *msg)
{
	dbus_bool_t ret = dbus_message_iter_next(&msg->top->iter);

	if(ret == FALSE) return false;

	return true;
}

void wdbus_message_get_basic(WDBusMessage *msg, void *value)
{
	// TODO: assert msg->type == WDBUS_MESSAGE_IN
	dbus_message_iter_get_basic(&msg->top->iter, value);
	wdbus_message_next_arg(msg);
}

int wdbus_message_append(WDBusMessage *msg, char type, const void *value)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %p, '%c', %p\n", __func__, msg, type, value);
	// TODO: assert msg->type == WDBUS_MESSAGE_OUT
	dbus_bool_t ret = dbus_message_iter_append_basic(&msg->top->iter, type, value);

	if(ret == FALSE) return 1;

	return 0;
}

int wdbus_message_recurse(WDBusMessage *msg, WDBusContainer *cont)
{
	int type;

	if(msg->type == WDBUS_MESSAGE_OUT) {
		// should use wdbus_container_open instead.
		return 1;
	}

	type = wdbus_message_get_arg_type(msg);
	if(type != DBUS_TYPE_ARRAY && type != DBUS_TYPE_VARIANT
			&& type != DBUS_TYPE_STRUCT && type != DBUS_TYPE_DICT_ENTRY) {
		// Avoid undefined behavior of recurse in non-container type
		return 1;
	}

	dbus_message_iter_recurse(&msg->top->iter, &cont->iter);

	cont->next = msg->top;
	msg->top = cont;

	return 0;
}

int wdbus_container_open(WDBusMessage *msg, WDBusContainer *cont, char type, const char *sign)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %p, %p, '%c', %s\n", __func__, msg, cont, type, sign);

	if(msg->type == WDBUS_MESSAGE_IN) {
		// should use wdbus_message_recurse instead.
		return 1;
	}
	dbus_bool_t ret = dbus_message_iter_open_container(&msg->top->iter, type,
			sign, &cont->iter);

	if(ret == FALSE) return 1;

	cont->next = msg->top;
	msg->top = cont;

	return 0;
}

int wdbus_container_close(WDBusMessage *msg)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %p\n", __func__, msg);

	if(!msg->top->next)
		return 0;

	if(msg->type == WDBUS_MESSAGE_OUT) {
		if(dbus_message_iter_close_container(&msg->top->next->iter,
					&msg->top->iter) == FALSE) {
			return 1;
		}
	}

	msg->top = msg->top->next;

	if(msg->type == WDBUS_MESSAGE_IN) {
		wdbus_message_next_arg(msg);
	}

	return 0;
}

int wdbus_container_close_all(WDBusMessage *msg)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %p: \n\t", __func__, msg);
	while(msg->top != &msg->bottom && !wdbus_container_close(msg))
		WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "\t");
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "\n");

	if(msg->top != &msg->bottom)
		return 1;

	return 0;
}

int wdbus_dict_append(WDBusMessage *msg, char key_type, const void *key, char value_type, const void *value)
{
	int ret = 1;
	const char value_type_as_string[2] = {value_type, '\0'};
	WDBusContainer entry, variant;

	if(wdbus_entry_open(msg, &entry)) {
		goto _ret;
	}

	if(wdbus_message_append(msg, key_type, key)) {
		goto _close_dict;
	}

	if(wdbus_variant_open(msg, &variant, value_type_as_string)) {
		goto _close_dict;
	}

	if(wdbus_message_append(msg, value_type, value)) {
		goto _close_variant;
	}

	ret = 0;

_close_variant:
	wdbus_container_close(msg);
_close_dict:
	wdbus_container_close(msg);
_ret:
	return ret;
}
