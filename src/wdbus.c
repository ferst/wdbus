#include "wdbus-private.h"

#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include <signal.h>
#include <sys/signalfd.h>

#include <sys/timerfd.h>

#include <sys/epoll.h>

// Event related types
typedef enum _WDBusEventType {
	WDBUS_WATCH_TYPE,
	WDBUS_TIMEOUT_TYPE,
	WDBUS_SIGNAL_TYPE,
	WDBusEventTypeSize
} WDBusEventType;

typedef struct _WDBusEventData {
	WDBusEventType type;
	int fd;
	union {
		DBusWatch *watch;
		DBusTimeout *timeout;
		struct signalfd_siginfo *sfsi;
	};
} WDBusEventData;

// Call related types
typedef struct _WDBusCallData {
	WDBusCallNotifyFunction callback;
	WDBusFreeCallback user_free;
	void *user_data;
} WDBusCallData;

// Context struct
struct _WDBusContext {
	DBusConnection *conn;
	int epfd, should_dispatch, nevent;
	struct signalfd_siginfo siginfo;
	WDBusEventData signal;
	WDBusFreeCallback user_free;
	void *user_data;
	struct epoll_event event[];
};

/*-----------------*/
/* Private methods */
/*-----------------*/
// Destructors
void wdbus_event_data_free(void *memory)
{
	WDBusEventData *ed = memory;

	if(ed->type == WDBUS_TIMEOUT_TYPE) {
		if(ed->fd != -1) {
			close(ed->fd);
		}
	}

	free(ed);
}

void wdbus_call_data_free(void *p)
{
	WDBusCallData *data = (WDBusCallData *)p;

	if(!data) {
		return;
	}

	if(data->user_free) {
		data->user_free(data->user_data);
	}

	free(data);
}

// Watch related methods
dbus_bool_t wdbus_add_watch(DBusWatch *watch, void *data)
{
	WDBusContext *ctx = data;
	WDBusEventData *ed;
	int flags, fd, ret = FALSE;
	struct epoll_event ev;

	fd = dbus_watch_get_unix_fd(watch);

	flags = dbus_watch_get_flags(watch);
	flags = (flags&DBUS_WATCH_READABLE?EPOLLIN:0)|(flags&DBUS_WATCH_WRITABLE?EPOLLOUT:0);

	ed = dbus_watch_get_data(watch);
	if(!ed) {
		ed = (WDBusEventData *)malloc(sizeof(WDBusEventData));//TODO: safe wdbus_new_event_data function
		if(!ed) {
			// TODO: log malloc fail
			goto _ret;
		}

		ed->type = WDBUS_WATCH_TYPE;
		ed->watch = watch;
		ed->fd = fd;

		dbus_watch_set_data(watch, ed, wdbus_event_data_free);
	}

	if(!dbus_watch_get_enabled(watch)) {
		ret = TRUE;
		goto _ret;
	}

	ev.events = flags;
	ev.data.ptr = ed;

	if(!epoll_ctl(ctx->epfd, EPOLL_CTL_ADD, fd, &ev)) {
		ret = TRUE;
		goto _ret;
	}

	// NOTE: If errno is EEXIST, another DBusWatch instance with the same
	// file descriptor are enabled. Documentations do not state that it is
	// impossible and this epoll structure rely on this. If it happens, this
	// code will need to be refactored. Good luck!

	// TODO: log EPOLL_CTL_ADD fail

	dbus_watch_set_data(watch, NULL, NULL);
	free(ed);

_ret:
	return ret;
}

void wdbus_watch_toggled(DBusWatch *watch, void *data)
{
	WDBusContext *ctx = data;
	WDBusEventData *ed;
	int fd, flags;
	struct epoll_event ev;

	if(dbus_watch_get_enabled(watch)) {
		fd = dbus_watch_get_unix_fd(watch);

		flags = dbus_watch_get_flags(watch);
		flags = (flags&DBUS_WATCH_READABLE?EPOLLIN:0)|(flags&DBUS_WATCH_WRITABLE?EPOLLOUT:0);

		ed = dbus_watch_get_data(watch);

		ev.events = flags;
		ev.data.ptr = ed;

		if(epoll_ctl(ctx->epfd, EPOLL_CTL_ADD, fd, &ev)){
			// TODO: log epoll_ctl_add fail.
			// Just log. We cannot fail here since this function returns void.
		}
	} else {
		fd = dbus_watch_get_unix_fd(watch);
		if(epoll_ctl(ctx->epfd, EPOLL_CTL_DEL, fd, NULL)){
			// TODO: log epoll_ctl_del fail.
			// Just log. We cannot fail here since this function returns void.
		}
	}
}

void wdbus_remove_watch(DBusWatch *watch, void *data)
{
	WDBusContext *ctx = data;
	WDBusEventData *ed;
	int fd;

	fd = dbus_watch_get_unix_fd(watch);
	ed = dbus_watch_get_data(watch);

	// TODO: assert ed.type == WDBUS_WATCH_TYPE

	if(epoll_ctl(ctx->epfd, EPOLL_CTL_DEL, fd, NULL)) {
		// TODO: log EPOLL_CTL_DEL fail (maybe ignore if errno == ENOENT)
	} else {
		ed->watch = NULL;
	}
}

// Timeout related methods
dbus_bool_t wdbus_add_timeout(DBusTimeout *timeout, void *data)
{
	WDBusContext *ctx = data;
	WDBusEventData *ed;
	int fd, time_ms, ret = FALSE;
	struct itimerspec value;
	struct epoll_event ev;

	ed = dbus_timeout_get_data(timeout);
	if(!ed) {
		ed = (WDBusEventData *)malloc(sizeof(WDBusEventData));
		if(!ed) {
			// TODO: log malloc fail
			goto _ret;
		}

		ed->type = WDBUS_TIMEOUT_TYPE;
		ed->timeout = timeout;
		ed->fd = -1;

		dbus_timeout_set_data(timeout, ed, wdbus_event_data_free);
	}

	if(ed->fd != -1) {
		fd = ed->fd;
	} else {
		fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);

		if(fd < 0) {
			//TODO: log timerfd_create fail
			goto _free;
		}

		ed->fd = fd;
	}

	if(!dbus_timeout_get_enabled(timeout)) {
		ret = TRUE;
		goto _ret;
	}

	time_ms = dbus_timeout_get_interval(timeout);

	value.it_value.tv_sec = time_ms/1000;
	value.it_value.tv_nsec = (time_ms - value.it_value.tv_sec*1e3)*1e6;

	value.it_interval.tv_sec = 0;
	value.it_interval.tv_nsec = 0;

	if(timerfd_settime(fd, 0, &value, NULL)) {
		// TODO: log timerfd_settime fail
		goto _close;
	}

	ev.events = EPOLLIN;
	ev.data.ptr = ed;

	if(!epoll_ctl(ctx->epfd, EPOLL_CTL_ADD, fd, &ev)){
		ret = TRUE;
		goto _ret;
	}

	// TODO: log epoll_ctl fail

_close:
	close(fd);
	ed->fd = -1;
_free:
	dbus_timeout_set_data(timeout, NULL, NULL);
	free(ed);
_ret:
	return ret;
}

void wdbus_timeout_toggled(DBusTimeout *timeout, void *data)
{
	WDBusEventData *ed;
	struct itimerspec value;
	int time_ms;

	ed = dbus_timeout_get_data(timeout);

	// TODO: assert ed.type == WDBUS_TIMEOUT_TYPE

	if(dbus_timeout_get_enabled(timeout)) {
		time_ms = dbus_timeout_get_interval(timeout); // return in ms

		value.it_value.tv_sec = time_ms/1000;
		value.it_value.tv_nsec = (time_ms - value.it_value.tv_sec*1e3)*1e6;
	} else {
		value.it_value.tv_sec = 0;
		value.it_value.tv_nsec = 0;
	}

	value.it_interval.tv_sec = 0;
	value.it_interval.tv_nsec = 0;

	timerfd_settime(ed->fd, 0, &value, NULL);
}

void wdbus_remove_timeout(DBusTimeout *timeout, void *data)
{
	WDBusEventData *ed;

	ed = dbus_timeout_get_data(timeout);

	// TODO: assert ed.type == WDBUS_TIMEOUT_TYPE

	if(ed->fd != -1) {
		close(ed->fd); // close removes fd from epfd (if no dup() was used, etc.)
		ed->timeout = NULL;
	}
}

// Other main-loop related methods
void wdbus_wakeup_main(void *data)
{
	WDBusContext *ctx = data;
	raise(SIGPOLL);
	ctx->should_dispatch = 1;
}

void wdbus_dispatch_status(DBusConnection *conn, DBusDispatchStatus new_status, void *data)
{
	WDBusContext *ctx = data;

	ctx->should_dispatch = new_status==DBUS_DISPATCH_DATA_REMAINS?1:0;
}

// Message methods
int wdbus_context_send_message(WDBusContext *ctx, WDBusMessage *msg)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %p, %p\n", __func__, ctx, msg);
	dbus_bool_t ret;

	if(wdbus_container_close_all(msg)) {
		return 1;
	}

	ret = dbus_connection_send(ctx->conn, msg->msg, NULL);
	dbus_message_unref(msg->msg);

	if(ret != TRUE) {
		return 1;
	}

	return 0;
}

// Call related methods
void wdbus_notify_pending_call(DBusPendingCall *pending, void *user_data)
{
	WDBusCallData *data = (WDBusCallData *)user_data;
	WDBusMessage msg = {.msg = dbus_pending_call_steal_reply(pending)};
	if(!msg.msg) {
		// TODO: log wdbus_notify_pending_call being called without reply.
		// Since it will be called by DBusWatch, there is no reason to hit this
		// point, I believe...
		return;
	}

	wdbus_message_init(&msg, msg.msg, WDBUS_MESSAGE_IN);

	if(data->callback) {
		data->callback(&msg, data->user_data);
	}

	dbus_message_unref(msg.msg);
	dbus_pending_call_unref(pending);
}

/*----------------*/
/* Public methods */
/*----------------*/
// Context creation and setup methods
WDBusContext *wdbus_context_create(int nevent)// TODO: remove epoll_event struct from WDBusContext
{
	WDBusContext *ctx = NULL;
	sigset_t set, oset;

	ctx = (WDBusContext *)malloc(sizeof(WDBusContext)+nevent*sizeof(struct epoll_event));
	if(!ctx) {
		//TODO: log malloc fail
		goto _ret;
	}

	ctx->nevent = nevent;
	ctx->user_data = NULL;
	ctx->user_free = NULL;

	ctx->epfd = epoll_create1(0);
	if(ctx->epfd < 0) {
		// TODO: log epoll_create1 fail
		goto _free;
	}

	if(sigemptyset(&set)) {
		// TODO: log sigemptyset (very unlikely)
		goto _free;
	}

	if(sigaddset(&set, SIGPOLL)) {
		// TODO: log sigaddset
		goto _free;
	}

	if(pthread_sigmask(SIG_BLOCK, &set, &oset)) {
		// TODO: log sigprocmask
		goto _free;
	}

	ctx->signal.fd = signalfd(-1, &set, SFD_NONBLOCK);
	if(ctx->signal.fd < 0) {
		// TODO: log signalfd fail
		goto _sig_restore;
	}

	ctx->signal.type = WDBUS_SIGNAL_TYPE;
	ctx->signal.sfsi = &ctx->siginfo;

	ctx->event[0].events = EPOLLIN;
	ctx->event[0].data.ptr = &ctx->signal;

	if(!epoll_ctl(ctx->epfd, EPOLL_CTL_ADD, ctx->signal.fd, &ctx->event[0])) {
		goto _ret;
	}

	// TODO: log epoll_ctl_add fail

	close(ctx->signal.fd);
_sig_restore:
	sigprocmask(SIG_SETMASK, &oset, NULL);
_free:
	free(ctx);
	ctx = NULL;
_ret:
	return ctx;
}

int wdbus_context_setup(WDBusContext *ctx)
{
	int ret = 1;

	if(!dbus_connection_set_watch_functions(ctx->conn, wdbus_add_watch,
			wdbus_remove_watch, wdbus_watch_toggled, ctx, NULL)) {
		// TODO: log dbus_connection_set_watch_functions fail.
		// The only reason to fail is no memory
		goto _ret;
	}

	if(!dbus_connection_set_timeout_functions(ctx->conn, wdbus_add_timeout,
			wdbus_remove_timeout, wdbus_timeout_toggled, ctx, NULL)) {
		// TODO: log dbus_connection_set_timeout_functions fail.
		// As in dbus_connection_set_watch_functions, the only reason to fail
		// is no memory.
		goto _ret;
	}

	dbus_connection_set_wakeup_main_function(ctx->conn, wdbus_wakeup_main, ctx, NULL);
	dbus_connection_set_dispatch_status_function(ctx->conn, wdbus_dispatch_status, ctx, NULL);

	while(dbus_connection_get_dispatch_status(ctx->conn) == DBUS_DISPATCH_DATA_REMAINS)
		dbus_connection_dispatch(ctx->conn);

	ret = 0;

_ret:
	return ret;
}

int wdbus_context_connect(WDBusContext *ctx, DBusBusType type, const char *name, unsigned int flags)
{
	int ret = 1;
	DBusError err;

	dbus_error_init(&err);
	ctx->conn = dbus_bus_get(type, &err);
	if(dbus_error_is_set(&err)) {
		//TODO: log dbus_bus_get_fail
		goto _ret;
	}

	if(name) {
		if(dbus_bus_request_name(ctx->conn, name, flags, &err) == DBUS_REQUEST_NAME_REPLY_EXISTS || dbus_error_is_set(&err)) {
			// TODO: log dbus_bus_request_name fail
			goto _unref;
		}
	}

	dbus_connection_set_exit_on_disconnect(ctx->conn, FALSE);

	ret = 0;

_unref:
	dbus_connection_unref(ctx->conn);
_ret:
	return ret;
}

// Setters and getters
void wdbus_context_set_user_data(WDBusContext *ctx, void *user_data, WDBusFreeCallback user_free)
{
	ctx->user_data = user_data;
	ctx->user_free = user_free;
}

void *wdbus_context_get_user_data(WDBusContext *ctx)
{
	return ctx->user_data;
}

int wdbus_context_get_epfd(WDBusContext *ctx)
{
	return ctx->epfd;
}

// Object related methods
int wdbus_context_register_object(WDBusContext *ctx, WDBusObject *obj)
{
	DBusError err;

	if(obj->ctx) {
		// TODO: log wdbus_context_register_object on already registered object
		return 1;
	}

	dbus_error_init(&err);
	dbus_connection_try_register_object_path(ctx->conn, obj->path, &wdbus_vt,
			obj, &err);

	if(dbus_error_is_set(&err)) {
		// TODO: log object registration fail. Use message from DBusError
		return 1;
	}

	obj->ctx = ctx;

	return 0;
}

int wdbus_context_unregister_object(WDBusContext *ctx, WDBusObject *obj)
{
	int ret = 1;

	if(dbus_connection_unregister_object_path(ctx->conn, obj->path) != FALSE) {
		ret = 0;
	}

	obj->ctx = NULL;

	return ret;
}

// Call-related methods
int wdbus_context_call(WDBusContext *ctx, WDBusMessage *msg, WDBusCallNotifyFunction callback, void *user_data, WDBusFreeCallback user_free, int timeout_ms)
{
	DBusPendingCall *pending;
	WDBusCallData *data;

	if(msg->type != WDBUS_MESSAGE_OUT) {
		// bad wmsg type
		goto _ret;
	}

	if(dbus_message_get_type(msg->msg) != DBUS_MESSAGE_TYPE_METHOD_CALL) {
		// bad msg type
		goto _ret;
	}

	data = (WDBusCallData *)malloc(sizeof(WDBusCallData));
	if(!data) {
		// no memory
		goto _ret;
	}

	data->callback = callback;
	data->user_data = user_data;
	data->user_free = user_free;

	if(dbus_connection_send_with_reply(ctx->conn, msg->msg, &pending, timeout_ms) == FALSE) {
		// no memory
		goto _free_data;
	}

	if(!pending) {
		// no memory
		goto _free_data;
	}

	// FIXME: race condition between dbus_connection_send_with_reply and
	// dbus_pending_call_set_notify. See bugzilla #19796, now gitlab issue #15

	if(dbus_pending_call_set_notify(pending, wdbus_notify_pending_call, data, wdbus_call_data_free) == FALSE) {
		// no memory
		goto _cancel_pending;
	}

	dbus_message_unref(msg->msg);

	return 0;

_cancel_pending:
	dbus_pending_call_cancel(pending);
_free_data:
	free(data);
_ret:
	return 1;
}

int wdbus_context_call_sync(WDBusContext *ctx, WDBusMessage *msg, int timeout_ms)
{
	DBusPendingCall *pending;
	DBusMessage *reply;

	if(msg->type != WDBUS_MESSAGE_OUT) {
		// bad wmsg type
		return 1;
	}

	if(dbus_message_get_type(msg->msg) != DBUS_MESSAGE_TYPE_METHOD_CALL) {
		// bad msg type
		return 1;
	}

	if(dbus_connection_send_with_reply(ctx->conn, msg->msg, &pending, timeout_ms) == FALSE) {
		// no memory
		return 1;
	}

	if(!pending) {
		// no memory
		return 1;
	}

	dbus_pending_call_block(pending);
	reply = dbus_pending_call_steal_reply(pending);
	dbus_pending_call_unref(pending);

	if(!reply) {
		// No message received.
		return 1;
	}

	dbus_message_unref(msg->msg);

	wdbus_message_init(msg, reply, WDBUS_MESSAGE_IN);

	return 0;
}

// Event loop methods
int wdbus_loop(WDBusContext *ctx, int timeout)
{
	int n, ret = 1;

	n = epoll_wait(ctx->epfd, ctx->event, ctx->nevent, timeout);
	if(n < 0) {
		//TODO: log epoll_wait fail.
		goto _ret;
	}

	while(n--) {
		switch(((WDBusEventData *)ctx->event[n].data.ptr)->type) {
			case WDBUS_SIGNAL_TYPE:
				read(ctx->signal.fd, ((WDBusEventData *)ctx->event[n].data.ptr)->sfsi, sizeof(struct signalfd_siginfo));

				if(dbus_connection_get_dispatch_status(ctx->conn) == DBUS_DISPATCH_DATA_REMAINS) {
					dbus_connection_ref(ctx->conn);
					dbus_connection_dispatch(ctx->conn);
					dbus_connection_unref(ctx->conn);
				}
				break;
			case WDBUS_TIMEOUT_TYPE:
				dbus_timeout_handle(((WDBusEventData *)ctx->event[n].data.ptr)->timeout);
				break;
			case WDBUS_WATCH_TYPE:
				dbus_connection_ref(ctx->conn);

				ctx->should_dispatch = 0;

				if(ctx->event[n].events&EPOLLIN) {
					dbus_watch_handle(((WDBusEventData *)ctx->event[n].data.ptr)->watch,
							DBUS_WATCH_READABLE);
				}

				if(ctx->event[n].events&EPOLLOUT) {
					dbus_watch_handle(((WDBusEventData *)ctx->event[n].data.ptr)->watch,
							DBUS_WATCH_WRITABLE);
				}

				if(ctx->event[n].events&EPOLLERR) {
					dbus_watch_handle(((WDBusEventData *)ctx->event[n].data.ptr)->watch,
							DBUS_WATCH_ERROR);
				}

				if(ctx->event[n].events&EPOLLHUP) {
					dbus_watch_handle(((WDBusEventData *)ctx->event[n].data.ptr)->watch,
							DBUS_WATCH_HANGUP);
				}

				if(ctx->should_dispatch) {
					while(dbus_connection_get_dispatch_status(ctx->conn) == DBUS_DISPATCH_DATA_REMAINS)
						dbus_connection_dispatch(ctx->conn);
					ctx->should_dispatch = 0;
				}

				dbus_connection_unref(ctx->conn);
				break;
			default:
				/*TODO: log unknown event type*/
				goto _ret;
		}
	}

	ret = 0;

_ret:
	return ret;
}

// Context destructor
void wdbus_context_free(WDBusContext *ctx)
{
	if(ctx->user_free) {
		ctx->user_free(ctx->user_data);
	}

	dbus_connection_flush(ctx->conn);
	//dbus_connection_unref(ctx->conn);
	dbus_shutdown();
	close(ctx->epfd);
	close(ctx->signal.fd);
	free(ctx);
}
