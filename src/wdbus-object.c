#include "wdbus-private.h"

#include <stdlib.h>
#include <string.h>

struct _WDBusInterfaceList {
	WDBusInterface *interface;
	struct _WDBusInterfaceList *next;
};

typedef enum _WDBusPropertyOperation {
	WDBUS_GET, WDBUS_GET_ALL, WDBUS_SET, WDBusPropertyOperationSize
} WDBusPropertyOperation;

// Defines

// Interface-related defines
#define WDBUS_XML_HEADER \
	"<!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\" \n"\
	"\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">\n"\
	"<!-- WDBus -->\n"\
	"<node>\n"

#define WDBUS_XML_FOOTER "</node>"

#define WDBUS_INTROSPECTABLE_INTERFACE \
	"  <interface name=\"org.freedesktop.DBus.Introspectable\">\n"\
	"    <method name=\"Introspect\">\n"\
	"      <arg name=\"data\" direction=\"out\" type=\"s\" />\n"\
	"    </method>\n"\
	"  </interface>\n"

#define WDBUS_PROPERTIES_INTERFACE \
	"  <interface name=\"org.freedesktop.DBus.Properties\">\n" \
	"    <method name=\"Get\">\n" \
	"      <arg type=\"s\" name=\"interface_name\" direction=\"in\"/>\n" \
	"      <arg type=\"s\" name=\"property_name\" direction=\"in\"/>\n" \
	"      <arg type=\"v\" name=\"value\" direction=\"out\"/>\n" \
	"    </method>\n" \
	"    <method name=\"GetAll\">\n" \
	"      <arg type=\"s\" name=\"interface_name\" direction=\"in\"/>\n" \
	"      <arg type=\"a{sv}\" name=\"properties\" direction=\"out\"/>\n" \
	"    </method>\n" \
	"    <method name=\"Set\">\n" \
	"      <arg type=\"s\" name=\"interface_name\" direction=\"in\"/>\n" \
	"      <arg type=\"s\" name=\"property_name\" direction=\"in\"/>\n" \
	"      <arg type=\"v\" name=\"value\" direction=\"in\"/>\n" \
	"    </method>\n" \
	"    <signal name=\"PropertiesChanged\">\n" \
	"      <arg type=\"s\" name=\"interface_name\"/>\n" \
	"      <arg type=\"a{sv}\" name=\"changed_properties\"/>\n" \
	"      <arg type=\"as\" name=\"invalidated_properties\"/>\n" \
	"    </signal>\n" \
	"  </interface>\n"

// Prototypes for data definition
// VTable Methos
DBusHandlerResult wdbus_message_function(DBusConnection *conn, DBusMessage *msg, void *user_data);
void wdbus_unregister_function(DBusConnection *conn, void *user_data);
// Interface Methods
DBusMessage *wdbus_introspect_method(WDBusObject *object, DBusMessage *request);
DBusMessage *wdbus_get_method(WDBusObject *object, DBusMessage *request);
DBusMessage *wdbus_get_all_method(WDBusObject *object, DBusMessage *request);
DBusMessage *wdbus_set_method(WDBusObject *object, DBusMessage *request);

/*---------*/
/* Globals */
/*---------*/

// Private data
DBusObjectPathVTable wdbus_vt = {
	.message_function = wdbus_message_function,
	.unregister_function = wdbus_unregister_function
};

// Public data
WDBusInterface wdbus_introspectable_interface = {
	.xml = WDBUS_INTROSPECTABLE_INTERFACE,
	.xml_len = strlen(WDBUS_INTROSPECTABLE_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("org.freedesktop.DBus.Introspectable"),
	.get = wdbus_properties_default_get,
	.get_all = wdbus_properties_default_get_all,
	.set = wdbus_properties_default_set,
	.user_data = NULL,
	.user_free = NULL,
	.nmethod = 1,
	.methods = {
		{.name = "Introspect", .callback = wdbus_introspect_method},
	}
};

WDBusInterface wdbus_properties_interface = {
	.xml = WDBUS_PROPERTIES_INTERFACE,
	.xml_len = strlen(WDBUS_PROPERTIES_INTERFACE),
	.name_len = strlen("org.freedesktop.DBus.Properties"),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.get = wdbus_properties_default_get,
	.get_all = wdbus_properties_default_get_all,
	.set = wdbus_properties_default_set,
	.user_data = NULL,
	.user_free = NULL,
	.nmethod = 3,
	.methods = {
		{.name = "Get", .callback = wdbus_get_method},
		{.name = "GetAll", .callback = wdbus_get_all_method},
		{.name = "Set", .callback = wdbus_set_method},
	}
};

/*-----------------*/
/* Private methods */
/*-----------------*/
WDBusInterface *wdbus_get_interface(WDBusObject *obj, const char *name)
{
	WDBusInterface *interface = NULL;
	size_t len;

	if(!obj || !obj->interfaces) {
		goto _ret;
	}

	len = strnlen(name, DBUS_MAXIMUM_NAME_LENGTH);

	// TODO: hash table of interface names?
	for(WDBusInterfaceList *list = obj->interfaces; list; list = list->next) {
		if(len == list->interface->name_len && !strncmp(name, &list->interface->xml[list->interface->name_offset], list->interface->name_len)) {
			interface = list->interface;
			break;
		}
	}

_ret:
	return interface;
}

// Interface methods
DBusMessage *wdbus_introspect_method(WDBusObject *object, DBusMessage *request)
{
	DBusMessage *reply;

	reply = dbus_message_new_method_return(request);
	if(object->xml_dirty) {
		wdbus_object_prepare_introspection(object);
		// TODO: warn user to call wdbus_object_prepare_introspection after
		// wdbus_object_register_interface to reduce introspection latency
	}

	dbus_message_append_args(reply, DBUS_TYPE_STRING, &object->xml, DBUS_TYPE_INVALID);

	return reply;
}

DBusMessage *wdbus_property_method(WDBusObject *object, DBusMessage *request, WDBusPropertyOperation op)
{
	WDBusInterface *interface;
	WDBusMessage req;
	WDBusContainer variant;
	DBusMessage *reply = NULL;
	char *name = NULL, *property = NULL;
	DBusError err;

	dbus_error_init(&err);
	if(wdbus_message_init(&req, request, WDBUS_MESSAGE_IN)) {
		reply = dbus_message_new_error_printf(request, DBUS_ERROR_INVALID_ARGS,
					"Method '%s' require %c arguments. Got none.",
					op==WDBUS_GET?"Get":op==WDBUS_GET_ALL?"GetAll":op==WDBUS_SET?"Set":"Unknown",
					op==WDBUS_GET?'2':op==WDBUS_GET_ALL?'1':op==WDBUS_SET?'3':'?');
		goto _ret;
	}

	if(wdbus_message_get_arg_type(&req) != DBUS_TYPE_STRING) {
		reply = dbus_message_new_error(request, DBUS_ERROR_INVALID_ARGS,
				"First argument type is wrong. Should be string.");
		goto _ret;
	}

	wdbus_message_get_basic(&req, &name);

	if(!name) {
		reply = dbus_message_new_error(request, DBUS_ERROR_INVALID_ARGS,
				"First argument is invalid. Null string.");
		goto _ret;
	}

	if(op != WDBUS_GET_ALL) {
		if(wdbus_message_get_arg_type(&req) != DBUS_TYPE_STRING) {
			// TODO: if wdbus_message_get_arg_type return DBUS_TYPE_INVALID,
			// the error message should be something like "Insufficient
			// arguments. Expected N, got 1."
			reply = dbus_message_new_error(request, DBUS_ERROR_INVALID_ARGS,
					"Second argument type is wrong. Should be string.");
			goto _ret;
		}

		wdbus_message_get_basic(&req, &property);

		if(op == WDBUS_SET) {
			if(wdbus_message_get_arg_type(&req) != DBUS_TYPE_VARIANT) {
				// TODO: same as previous TODO, with message like "Insufficient
				// arguments. Expected 3, got 2."
				reply = dbus_message_new_error(request, DBUS_ERROR_INVALID_ARGS,
						"Third argument type is wrong. Should be variant.");
				goto _ret;
			}

			wdbus_message_recurse(&req, &variant);
		}
	}

	interface = wdbus_get_interface(object, name);
	if(!interface) {
		reply = dbus_message_new_error(request, DBUS_ERROR_INVALID_ARGS, "Unknow interface");
		goto _ret;
	}

	switch(op) {
		case WDBUS_GET:
			reply = interface->get(object, interface, property, &req);
			break;
		case WDBUS_GET_ALL:
			reply = interface->get_all(object, interface, property, &req);
			break;
		case WDBUS_SET:
			reply = interface->set(object, interface, property, &req);
			break;
		case WDBusPropertyOperationSize:
		default:
			// TODO: log invalid operation
			reply = dbus_message_new_error(request, DBUS_ERROR_FAILED, "Invalid Operation");
	}

_ret:
	return reply;
}

DBusMessage *wdbus_get_method(WDBusObject *object, DBusMessage *request)
{
	return wdbus_property_method(object, request, WDBUS_GET);
}

DBusMessage *wdbus_get_all_method(WDBusObject *object, DBusMessage *request)
{
	return wdbus_property_method(object, request, WDBUS_GET_ALL);
}

DBusMessage *wdbus_set_method(WDBusObject *object, DBusMessage *request)
{
	return wdbus_property_method(object, request, WDBUS_SET);
}

// VTable methods
DBusHandlerResult wdbus_message_function(DBusConnection *conn, DBusMessage *msg, void *user_data)
{
	DBusHandlerResult ret = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	DBusMessage *reply = NULL;
	WDBusObject *obj = user_data;
	WDBusInterface *interface;
	int i;
	const char	*name = dbus_message_get_interface(msg),
				*method = dbus_message_get_member(msg);

	if(!method) {
		// TODO: log
		goto _ret;
	}

	interface = wdbus_get_interface(obj, name);
	if(!interface) {
		// TODO: log interface not found
		goto _ret;
	}

	for(i = 0; i < interface->nmethod; i++) {
		if(!strcmp(method, interface->methods[i].name)) {
			reply = interface->methods[i].callback(obj, msg);
			break;
		}
	}

	if(i == interface->nmethod) {
		// TODO: log method not found
		goto _ret;
	}

	dbus_connection_send(conn, reply, NULL);
	dbus_message_unref(reply);
	ret = DBUS_HANDLER_RESULT_HANDLED;

_ret:
	return ret;
}

void wdbus_unregister_function(DBusConnection *conn, void *user_data)
{
	wdbus_object_free(user_data);
}

/*----------------*/
/* Public methods */
/*----------------*/
// Interface methods
// Default properties methods
DBusMessage *wdbus_properties_default_get(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	return dbus_message_new_error_printf(request->msg, DBUS_ERROR_UNKNOWN_PROPERTY, "Property \"%*s.%s\" not found.", interface->name_len, &interface->xml[interface->name_offset], property);
}

DBusMessage *wdbus_properties_default_get_all(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	WDBusMessage reply;
	WDBusContainer dict;

	reply.msg = dbus_message_new_method_return(request->msg);
	wdbus_message_init(&reply, reply.msg, WDBUS_MESSAGE_OUT);
	wdbus_dict_open(&reply, &dict, DBUS_TYPE_STRING_AS_STRING, DBUS_TYPE_VARIANT_AS_STRING);
	wdbus_container_close(&reply);

	return reply.msg;
}

DBusMessage *wdbus_properties_default_set(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	return dbus_message_new_error_printf(request->msg, DBUS_ERROR_UNKNOWN_PROPERTY, "Property \"%*s.%s\" not found.", interface->name_len, &interface->xml[interface->name_offset], property);
}

// Object methods
WDBusObject *wdbus_object_create(const char *path)
{
	WDBusObject *obj = NULL;
	int size;

	obj = (WDBusObject *)malloc(sizeof(WDBusObject));
	if(!obj) {
		// TODO: log malloc object fail
		goto _ret;
	}

	obj->user_data = NULL;
	obj->user_free = NULL;

	obj->path = strndup(path, WDBUS_MAXIMUM_PATH_LENGTH);
	if(!obj->path) {
		// TODO: log path malloc fail
		goto _free_object;
	}

	size = strlen(WDBUS_XML_HEADER) + strlen(WDBUS_XML_FOOTER) + 1;
	obj->xml = (char *)malloc(sizeof(char)*size);
	if(!obj->xml) {
		// TODO: log xml malloc fail
		goto _free_path;
	}

	strcpy(obj->xml, WDBUS_XML_HEADER);
	strcpy(obj->xml+strlen(WDBUS_XML_HEADER), WDBUS_XML_FOOTER);

	obj->interfaces = NULL;
	obj->xml_dirty = 0;
	obj->ctx = NULL;

	goto _ret;
	
_free_path:
	free(obj->path);
_free_object:
	free(obj);
	obj = NULL;
_ret:
	return obj;
}

void wdbus_object_set_user_data(WDBusObject *obj, void *user_data, WDBusFreeCallback user_free)
{
	obj->user_data = user_data;
	obj->user_free = user_free;
}

void *wdbus_object_get_user_data(WDBusObject *obj)
{
	return obj->user_data;
}

void *wdbus_object_get_context_data(WDBusObject *obj)
{
	if(!obj->ctx) {
		// Object was not registered in any context
		return NULL;
	}
	return wdbus_context_get_user_data(obj->ctx);
}

const char *wdbus_object_get_path(WDBusObject *obj)
{
	return obj->path;
}

int wdbus_object_register_interface(WDBusObject *obj, WDBusInterface *interface)
{
	int ret = 1;
	WDBusInterfaceList *new, *last;

	new = (WDBusInterfaceList *)malloc(sizeof(WDBusInterfaceList));
	if(!new) {
		// TODO: log interface list malloc fail
		goto _ret;
	}

	new->next = NULL;
	new->interface = interface;

	if(obj->interfaces == NULL) {
		obj->interfaces = new;
	} else {
		for(last = obj->interfaces; last->next; last = last->next);
		last->next = new;
	}

	obj->xml_dirty = 1;
	ret = 0;

_ret:
	return ret;
}

int wdbus_object_prepare_introspection(WDBusObject *obj)
{
	int ret = 1, size = strlen(WDBUS_XML_HEADER) + strlen(WDBUS_XML_FOOTER) + 1;
	char *ptr;

	if(!obj->xml_dirty) {
		ret = 0;
		goto _ret;
	}

	for(WDBusInterfaceList *list = obj->interfaces; list; list = list->next) {
		size += list->interface->xml_len;
	}

	ptr = realloc(obj->xml, size);
	if(!ptr) {
		// TODO: log realloc fail
		goto _ret;
	}

	obj->xml = ptr;

	strcpy(obj->xml, WDBUS_XML_HEADER);
	ptr += strlen(WDBUS_XML_HEADER);

	for(WDBusInterfaceList *list = obj->interfaces; list; list = list->next) {
		strcpy(ptr, list->interface->xml);//TODO: strncpy
		ptr += list->interface->xml_len;
	}

	strcpy(ptr, WDBUS_XML_FOOTER);

	obj->xml_dirty = 0;
	ret = 0;

_ret:
	return ret;
}

int wdbus_object_emit_signal(WDBusObject *obj, WDBusMessage *signal)
{
	WDBUS_LOG(WDBUS_LOG_LEVEL_TRACE, "%s: %s, %p\n", __func__, obj->path, signal);
	if(!obj->ctx) {
		// Object was not registered in any context
		return 1;
	}
	return wdbus_context_send_message(obj->ctx, signal);
}

void wdbus_object_free(WDBusObject *obj)
{
	WDBusInterfaceList *list;

	if(obj->user_free) {
		obj->user_free(obj->user_data);
	}

	while(obj->interfaces) {
		list = obj->interfaces;
		obj->interfaces = obj->interfaces->next;
		free(list);
	}

	free(obj->xml);
	free(obj->path);
	free(obj);
}
