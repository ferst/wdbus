# WDBus

WDBus is a small [DBus](https://www.freedesktop.org/wiki/Software/dbus/)
binding in C that depends on libdbus only. Other options, such as
[GDBus](http://library.gnome.org/devel/gio/stable/gdbus-lowlevel.html),
[Eldbus](https://docs.enlightenment.org/efl/current/eldbus_main.html) or
[sd-bus](https://www.freedesktop.org/software/systemd/man/sd-bus.html) pull
rather large dependencies like GNOME's
[GLib](https://gitlab.gnome.org/GNOME/glib), Enlightenment's
[EFL](https://git.enlightenment.org/core/efl.git/tree/src/lib/eldbus) or
[systemd](https://github.com/systemd/systemd/), respectively.

If you already depend on one of these, everything is OK; otherwise, it will be
a big effort, particularly if you are building all packages from source, say,
for a [Buildroot](https://www.buildroot.org/) project.

The project started as an example of main loop with epoll, and grew to handle
interfaces and objects at service side. The client-side facilities are WIP,
notably missing signal match.

## Getting started

Just clone this repository and run make
```shell
git clone https://gitlab.com/ferst/wdbus
cd wdbus
make
```
to get `libwdbus.so` under `lib` directory.

To build examples use
```shell
make examples
```

Define `LIBDIR` and `INCLUDEDIR` to install:
```shell
make LIBDIR=... INCLUDEDIR=... install
```
