/* SyncClient1.c: Synchronous call
 *
 * This example shows how to call the "Hello" method of HelloWorld1 example
 * synchronously with the wdbus_context_cal_sync method. A WDBusContext
 * is created and connected to the session bus. Then, we'll create a method
 * call message and send it with wdbus_context_cal_sync. Note that unlike the
 * Client1.c example, there is no need to call wdbus_context_setup if we don't
 * have a main loop with wdbus_loop.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Enqueue message and block
	if(wdbus_context_call_sync(ctx, &call, -1)) {
		printf("wdbus_context_call_sync error.\n");
		return 1;
	}

	printf("Hello reply received.\n");

	dbus_message_unref(call.msg);

	wdbus_context_free(ctx);

	return 0;
}

