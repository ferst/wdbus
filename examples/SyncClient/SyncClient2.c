/* SyncClient2.c: Using "Hello" reply
 *
 * Expanding on the previous example, we'll now talk to the HelloWorld2
 * service, which responds to the "Hello" method with a string. After
 * wdbus_context_call_sync, we'll check whether the returned message is an
 * error, then checks the type of the reply argument, and finally retrieves
 * and prints the string.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	char *resp;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld2", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Enqueue message and block
	if(wdbus_context_call_sync(ctx, &call, -1)) {
		printf("wdbus_context_call_sync error.\n");
		return 1;
	}

	// The call message was replaced by the reply message in "call" struct.
	// Check if the message is an error
	if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(call.msg));
		return 1;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		return 1;
	}

	// Get argument
	wdbus_message_get_basic(&call, &resp);

	printf("Hello reply: %s", resp?resp:"NULL");

	// The caller of wdbus_context_call_sync is responsible for decreasing the
	// message reference count.
	dbus_message_unref(call.msg);

	wdbus_context_free(ctx);

	return 0;
}

