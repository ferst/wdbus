/* SyncClient5.c: Setting properties
 *
 * In this example we'll expand SyncClient5.c to Get and Set the new property
 * of HelloWorld5: Excited. After printing the received value of
 * NumberOfGreetings, we'll call Get on Excited property, invert the value read
 * and call the Set method. Also, the calls are now in a loop in the global
 * variable "stop", which is changed on signal_handler that is attached to
 * SIGINT.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	WDBusContainer variant;
	int32_t NumberOfGreetings;
	char *buf;
	dbus_bool_t bool_val; // DBus boolean is 4 bytes due to the wire protocol

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	while(!stop) {
		// Prepare message to call "Hello"
		wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
					"/br/wdbus/Example", "br.wdbus.Example.HelloWorld5", "Hello"),
				WDBUS_MESSAGE_OUT);

		// Append first argument
		buf = "WDBus SyncClient";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		// Enqueue message and block
		if(wdbus_context_call_sync(ctx, &call, -1)) {
			printf("wdbus_context_call_sync error.\n");
			stop = true;
			continue;
		}

		// The call message was replaced by the reply message in "call" struct.
		// Check if the message is an error
		if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
			printf("Hello reply: %s.\n", dbus_message_get_error_name(call.msg));
			stop = true;
			continue;
		}

		// Check argument type
		if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_STRING) {
			printf("Hello reply: bad argument type.\n");
			stop = true;
			continue;
		}

		// Get argument
		wdbus_message_get_basic(&call, &buf);

		printf("Hello reply: %s", buf?buf:"NULL");

		// The caller of wdbus_context_call_sync is responsible for decreasing the
		// message reference count.
		dbus_message_unref(call.msg);

		// Reuse the "call" struct to prepare the call to Get
		wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
					"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
				WDBUS_MESSAGE_OUT);

		// Append interface name
		buf = "br.wdbus.Example.HelloWorld5";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		// Append property name
		buf = "NumberOfGreetings";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		// Enqueue message and block
		if(wdbus_context_call_sync(ctx, &call, -1)) {
			printf("wdbus_context_call_sync error.\n");
			stop = true;
			continue;
		}

		// Check if the message is an error
		if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
			printf("Get NumberOfGreetings reply: %s.\n", dbus_message_get_error_name(call.msg));
			stop = true;
			continue;
		}

		// Check argument type
		if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_VARIANT) {
			printf("Get NumberOfGreetings reply: bad argument type.\n");
			stop = true;
			continue;
		}

		// Open variant
		wdbus_message_recurse(&call, &variant);

		// Check variant's content type
		if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_INT32) {
			printf("Get NumberOfGreetings reply: bad variant content type.\n");
			stop = true;
			continue;
		}

		// Get argument
		wdbus_message_get_basic(&call, &NumberOfGreetings);

		printf("Number of greetings: %d.\n", NumberOfGreetings);

		// Again, the caller of wdbus_context_call_sync should unref the reply
		// message.
		dbus_message_unref(call.msg);

		// Call Get on "Excited" property
		wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
					"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
				WDBUS_MESSAGE_OUT);

		buf = "br.wdbus.Example.HelloWorld5";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		buf = "Excited";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		// Enqueue message and block
		if(wdbus_context_call_sync(ctx, &call, -1)) {
			printf("wdbus_context_call_sync error.\n");
			stop = true;
			continue;
		}

		// Check if the message is an error
		if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
			printf("Get Excited reply: %s.\n", dbus_message_get_error_name(call.msg));
			stop = true;
			continue;
		}

		// Check argument type
		if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_VARIANT) {
			printf("Get Excited reply: bad argument type.\n");
			stop = true;
			continue;
		}

		wdbus_message_recurse(&call, &variant);

		if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_BOOLEAN) {
			printf("Get Excited reply: bad variant content type.\n");
			stop = true;
			continue;
		}

		// Get argument
		wdbus_message_get_basic(&call, &bool_val);

		printf("Excited: %s.\n", bool_val?"TRUE":"FALSE");

		// Unref reply message
		dbus_message_unref(call.msg);

		// Toggles Excited
		bool_val = !bool_val;

		// Prepare message to call Set with the new value
		wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
					"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Set"),
				WDBUS_MESSAGE_OUT);

		// Append interface name
		buf = "br.wdbus.Example.HelloWorld5";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		// Append property name
		buf = "Excited";
		wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

		// Open variant container
		wdbus_variant_open(&call, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);

		// Append argument
		wdbus_message_append(&call, DBUS_TYPE_BOOLEAN, &bool_val);

		// Close variant container
		wdbus_container_close(&call);

		// Enqueue message and block
		if(wdbus_context_call_sync(ctx, &call, -1)) {
			printf("wdbus_context_call_sync error.\n");
			stop = true;
			continue;
		}

		// Check for errors
		if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
			printf("Set Excited reply: %s.\n", dbus_message_get_error_name(call.msg));
			stop = true;
			continue;
		}

		printf("Excited value toggled.\n");

		// Unref reply message
		dbus_message_unref(call.msg);
	}

	wdbus_context_free(ctx);

	return 0;
}

