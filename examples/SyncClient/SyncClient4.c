/* SyncClient4.c: Getting properties
 *
 * Proceeding from SyncClient3.c, this example will interact with the new
 * property added in HelloWorld4, NumberOfGreetings. After printing the answer
 * of "Hello", the Get method of org.freedesktop.DBus.Properties will be called
 * to receive this property.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	WDBusContainer variant;
	int32_t NumberOfGreetings;
	char string[] = "WDBus SyncClient", *buf = string;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld4", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Append first argument
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message and block
	if(wdbus_context_call_sync(ctx, &call, -1)) {
		printf("wdbus_context_call_sync error.\n");
		return 1;
	}

	// The call message was replaced by the reply message in "call" struct.
	// Check if the message is an error
	if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(call.msg));
		return 1;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		return 1;
	}

	// Get argument
	wdbus_message_get_basic(&call, &buf);

	printf("Hello reply: %s", buf?buf:"NULL");

	// The caller of wdbus_context_call_sync is responsible for decreasing the
	// message reference count.
	dbus_message_unref(call.msg);

	// Reuse the "call" struct to prepare the call to Get
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
			WDBUS_MESSAGE_OUT);

	// Append interface name
	buf = "br.wdbus.Example.HelloWorld4";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Append property name
	buf = "NumberOfGreetings";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message and block
	if(wdbus_context_call_sync(ctx, &call, -1)) {
		printf("wdbus_context_call_sync error.\n");
		return 1;
	}

	// Check if the message is an error
	if(dbus_message_get_type(call.msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Get reply: %s.\n", dbus_message_get_error_name(call.msg));
		return 1;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_VARIANT) {
		printf("Get reply: bad argument type.\n");
		return 1;
	}

	// Open variant
	wdbus_message_recurse(&call, &variant);

	// Check variant's content type
	if(wdbus_message_get_arg_type(&call) != DBUS_TYPE_INT32) {
		printf("Get reply: bad variant content type.\n");
		return 1;
	}

	// Get argument
	wdbus_message_get_basic(&call, &NumberOfGreetings);

	printf("Number of greetings: %d.\n", NumberOfGreetings);

	// Again, the caller of wdbus_context_call_sync should unref the reply
	// message.
	dbus_message_unref(call.msg);

	wdbus_context_free(ctx);

	return 0;
}

