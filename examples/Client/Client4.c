/* Client4.c: Getting properties
 *
 * Proceeding from Client3.c, this example will interact with the new property
 * added in HelloWorld4, NumberOfGreetings. At the end of hello_reply_callback,
 * the Get method of org.freedesktop.DBus.Properties will be called to receive
 * this property. A new callback, get_reply_callback, will be created to handle
 * the response for this second call.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// Callback prototypes
void hello_reply_callback(WDBusMessage *msg, void *user_data);
void get_reply_callback(WDBusMessage *msg, void *user_data);

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	char string[] = "WDBus Client";
	char *buf = string;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld4", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Append first argument
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message to be sent
	wdbus_context_call(ctx, &call, hello_reply_callback, ctx, NULL, -1);

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	wdbus_context_free(ctx);

	return 0;
}

// Callback for "Hello" reply
void hello_reply_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContext *ctx = user_data;
	WDBusMessage call;
	char *buf;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &buf);

	printf("Hello reply: %s", buf?buf:"NULL");

	// Prepare message to call Get on NumberOfGreetings
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld4";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "NumberOfGreetings";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message
	wdbus_context_call(ctx, &call, get_reply_callback, NULL, NULL, -1);
}

// Callback for Get reply
void get_reply_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContainer variant;
	int32_t NumberOfGreetings;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Get reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_VARIANT) {
		printf("Get reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Open variant
	wdbus_message_recurse(msg, &variant);

	// Check variant's content type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_INT32) {
		printf("Get reply: bad variant content type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &NumberOfGreetings);

	printf("Number of greetings: %d.\n", NumberOfGreetings);

	stop = true;
	return;
}

