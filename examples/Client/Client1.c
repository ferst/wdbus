/* Client1.c: Asynchronous call
 *
 * This example shows how to call the "Hello" method of HelloWorld1 example
 * with the wdbus_context_call method. First, a WDBusContext is created and
 * connected to the session bus. Then, we create a method call message and
 * send it with wdbus_context_call. The third argument is the callback function
 * that will be invoked when the service reply is received.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// Callback prototype
void hello_reply_callback(WDBusMessage *msg, void *user_data);

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Enqueue message to be sent
	wdbus_context_call(ctx, &call, hello_reply_callback, NULL, NULL, -1);

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Clean up
	wdbus_context_free(ctx);

	return 0;
}

// Callback for "Hello" reply
void hello_reply_callback(WDBusMessage *msg, void *user_data)
{
	printf("Hello reply received.\n");
	stop = true;
}

