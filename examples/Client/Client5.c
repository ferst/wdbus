/* Client5.c: Setting properties
 *
 * In this example we'll expand Client4.c to Get and Set the new property of
 * HelloWorld5: Excited. Two new callbacks will be created,
 * get_excited_reply_callback and set_excited_reply_callback. We'll call Get at
 * the end of hello_reply_callback, invert the value read in
 * get_excited_reply_callback and call the Set method. Finally, at the end of
 * set_excited_reply_callback, we'll call Hello again, creating a loop until
 * signal_handler changes stop value.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// Callback prototype
void hello_resp_callback(WDBusMessage *msg, void *user_data);
void get_numberofgreetings_resp_callback(WDBusMessage *msg, void *user_data);
void get_excited_resp_callback(WDBusMessage *msg, void *user_data);
void set_excited_resp_callback(WDBusMessage *msg, void *user_data);

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	char string[] = "WDBus Client";
	char *buf = string;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld5", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Append first argument
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message to be sent
	wdbus_context_call(ctx, &call, hello_resp_callback, ctx, NULL, -1);

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	wdbus_context_free(ctx);

	return 0;
}

// Callback for "Hello" reply
void hello_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContext *ctx = user_data;
	WDBusMessage call;
	char *buf;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &buf);

	printf("Hello reply received: %s", buf?buf:"NULL");

	// Prepare message to call Get on NumberOfGreetings
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld5";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "NumberOfGreetings";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message
	wdbus_context_call(ctx, &call, get_numberofgreetings_resp_callback, NULL, NULL, -1);

	// Prepare message to call Get on Excited
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld5";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "Excited";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message
	wdbus_context_call(ctx, &call, get_excited_resp_callback, ctx, NULL, -1);
}

// Callback for Get on NumberOfGreetings
void get_numberofgreetings_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContainer variant;
	int32_t NumberOfGreetings;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Get NumberOfGreetings reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_VARIANT) {
		printf("Get NumberOfGreetings reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Open variant
	wdbus_message_recurse(msg, &variant);

	// Check variant's argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_INT32) {
		printf("Get NumberOfGreetings reply: bad variant content type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &NumberOfGreetings);

	printf("Number of greetings: %d.\n", NumberOfGreetings);
}

// Callback for Get on Excited
void get_excited_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContext *ctx = user_data;
	WDBusContainer variant;
	WDBusMessage call;
	dbus_bool_t bool_val; // DBus boolean is 4 bytes due to the wire protocol
	char *buf;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Get Excited reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_VARIANT) {
		printf("Get Excited reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Open variant
	wdbus_message_recurse(msg, &variant);

	// Check variant's argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_BOOLEAN) {
		printf("Get Excited reply: bad variant content type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &bool_val);

	printf("Excited: %s.\n", bool_val?"TRUE":"FALSE");

	// Prepare message to call Set with inverted value
	bool_val = !bool_val;

	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Set"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld5";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "Excited";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Open variant container
	wdbus_variant_open(&call, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);

	// Append argument
	wdbus_message_append(&call, DBUS_TYPE_BOOLEAN, &bool_val);

	// Close variant container
	wdbus_container_close(&call);

	// Enqueue message
	wdbus_context_call(ctx, &call, set_excited_resp_callback, ctx, NULL, -1);
}

// Callback for Set on Excited
void set_excited_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContext *ctx = user_data;
	WDBusMessage call;
	char *buf = "Set callback";

	// Check if the message is and error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Set Excited reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	printf("Excited value toggled.\n");

	// Prepare message to call Hello again
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld5", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Append first argument
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message
	wdbus_context_call(ctx, &call, hello_resp_callback, ctx, NULL, -1);
}

