/* Client3.c: Calling methods with arguments
 *
 * This example will talk to the HelloWorld3 service, whose "Hello" method now
 * requires an argument, which will be appended to the calling message with
 * wdbus_message_append.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// Callback prototype
void hello_reply_callback(WDBusMessage *msg, void *user_data);

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	char string[] = "WDBus Client";
	char *buf = string;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld3", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Append first argument
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message to be sent
	wdbus_context_call(ctx, &call, hello_reply_callback, NULL, NULL, -1);

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	wdbus_context_free(ctx);

	return 0;
}

// Callback for "Hello" reply
void hello_reply_callback(WDBusMessage *msg, void *user_data)
{
	char *resp;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &resp);

	printf("Hello reply: %s", resp?resp:"NULL");
	stop = true;
}

