/* Client2.c: Handling "Hello" reply
 *
 * Expanding on the previous example, we'll now talk to the HelloWorld2
 * service, which responds to the "Hello" method with a string. The new
 * hello_reply_callback will check whether the returned message is an error,
 * then checks the type of the reply argument, and finally retrieves and prints
 * the string.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// Callback prototype
void hello_resp_callback(WDBusMessage *msg, void *user_data);

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Prepare message to call "Hello"
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "br.wdbus.Example.HelloWorld2", "Hello"),
			WDBUS_MESSAGE_OUT);

	// Enqueue message to be sent
	wdbus_context_call(ctx, &call, hello_resp_callback, NULL, NULL, -1);

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Clean up
	wdbus_context_free(ctx);

	return 0;
}

// Callback for "Hello" reply
void hello_resp_callback(WDBusMessage *msg, void *user_data)
{
	char *resp;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &resp);

	printf("Hello reply: %s", resp?resp:"NULL");
	stop = true;
}

