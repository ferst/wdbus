/* Client6.c: User's main loop
 *
 * This example will add an application main loop using epoll. This epoll
 * instance will monitor the STDIN and wdbus' epfd. When the first becomes
 * ready, we'll read user's input and create a Hello call with this data; when
 * the other becomes ready, we'll use wdbus_loop.
 */
#include "wdbus.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/epoll.h>
#include <unistd.h>

// Callbacks
void hello_resp_callback(WDBusMessage *msg, void *user_data);
void get_numberofgreetings_resp_callback(WDBusMessage *msg, void *user_data);
void get_excited_resp_callback(WDBusMessage *msg, void *user_data);
void set_excited_resp_callback(WDBusMessage *msg, void *user_data);

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo) {
	stop = true;
}

int main(int argc, char *argv[])
{
	WDBusContext *ctx;
	WDBusMessage call;
	int epfd, n;
	struct epoll_event event = {.events = EPOLLIN};
	char buffer[256], *buf = buffer;

	// Create our epoll
	epfd = epoll_create1(0);
	if(epfd < 0) {
		printf("epoll_create1 fail.\n");
		return 1;
	}

	// Add stdin to our epoll
	event.data.fd = STDIN_FILENO;
	if(epoll_ctl(epfd, EPOLL_CTL_ADD, event.data.fd, &event)) {
		printf("epoll_ctl stdin fail.\n");
		return 1;
	}

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Add context epfd to our epoll
	event.data.fd = wdbus_context_get_epfd(ctx);
	if(epoll_ctl(epfd, EPOLL_CTL_ADD, event.data.fd, &event)) {
		printf("epoll_ctl context_epfd fail.\n");
		return 1;
	}

	// Connect without name request
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, NULL, 0)) {
		printf("wdbus_context_connect fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		n = epoll_wait(epfd, &event, 1, -1);
		if(n < 0) {
			// You should handle errno here
			continue;
		}

		if(event.data.fd == STDIN_FILENO) {
			n = read(STDIN_FILENO, buffer, sizeof(buffer)-1);
			if(n > 0) {
				// Prepare buffer
				buffer[n] = '\0';
				if(buffer[n-1] == '\n') {
					buffer[n-1] = '\0';
				}

				// Prepare message to call Hello
				wdbus_message_init(&call, dbus_message_new_method_call(
							"br.wdbus.Example", "/br/wdbus/Example",
							"br.wdbus.Example.HelloWorld5", "Hello"),
						WDBUS_MESSAGE_OUT);

				// Append first argument
				wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

				// Enqueue message
				wdbus_context_call(ctx, &call, hello_resp_callback, ctx, NULL, -1);
			} else {
				stop = true;
				continue;
			}
		} else {
			// wdbus event
			wdbus_loop(ctx, 0);
		}
	}

	// Cleanup
	printf("Signal caught. Exiting...");

	wdbus_context_free(ctx);

	printf("OK.\n");

	return 0;
}

// Callback for "Hello" reply
void hello_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContext *ctx = user_data;
	WDBusMessage call;
	char *buf;

	// Check if the message is an errors
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Hello reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_STRING) {
		printf("Hello reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &buf);

	printf("Hello reply received: %s", buf?buf:"NULL");

	// Prepare message to call Get on NumberOfGreetings
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld5";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "NumberOfGreetings";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message
	wdbus_context_call(ctx, &call, get_numberofgreetings_resp_callback, NULL, NULL, -1);

	// Prepare message to call Get on Excited
	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Get"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld5";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "Excited";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Enqueue message
	wdbus_context_call(ctx, &call, get_excited_resp_callback, ctx, NULL, -1);
}

// Callback for Get on NumberOfGreetings
void get_numberofgreetings_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContainer variant;
	int32_t NumberOfGreetings;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Get NumberOfGreetings reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_VARIANT) {
		printf("Get NumberOfGreetings reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Open variant
	wdbus_message_recurse(msg, &variant);

	// Check variant's content type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_INT32) {
		printf("Get NumberOfGreetings reply: bad variant content type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &NumberOfGreetings);

	printf("Number of greetings: %d.\n", NumberOfGreetings);
}

// Callback for Get on Excited
void get_excited_resp_callback(WDBusMessage *msg, void *user_data)
{
	WDBusContext *ctx = user_data;
	WDBusContainer variant;
	WDBusMessage call;
	dbus_bool_t bool_val; // DBus boolean is 4 bytes due to the wire protocol
	char *buf;

	// Check if the message is an error
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Get Excited reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	// Check argument type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_VARIANT) {
		printf("Get Excited reply: bad argument type.\n");
		stop = true;
		return;
	}

	// Open variant
	wdbus_message_recurse(msg, &variant);

	// Check variant's content type
	if(wdbus_message_get_arg_type(msg) != DBUS_TYPE_BOOLEAN) {
		printf("Get Excited reply: bad variant content type.\n");
		stop = true;
		return;
	}

	// Get argument
	wdbus_message_get_basic(msg, &bool_val);

	printf("Excited: %s.\n", bool_val?"TRUE":"FALSE");

	// Prepare message to call Set with inverted value
	bool_val = !bool_val;

	wdbus_message_init(&call, dbus_message_new_method_call("br.wdbus.Example",
				"/br/wdbus/Example", "org.freedesktop.DBus.Properties", "Set"),
			WDBUS_MESSAGE_OUT);

	buf = "br.wdbus.Example.HelloWorld5";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	buf = "Excited";
	wdbus_message_append(&call, DBUS_TYPE_STRING, &buf);

	// Open variant conatiner
	wdbus_variant_open(&call, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);

	// Append argument
	wdbus_message_append(&call, DBUS_TYPE_BOOLEAN, &bool_val);

	// Close variant container
	wdbus_container_close(&call);

	// Enqueue message
	wdbus_context_call(ctx, &call, set_excited_resp_callback, NULL, NULL, -1);
}

// Callback for Set on Excited
void set_excited_resp_callback(WDBusMessage *msg, void *user_data)
{
	// Check for errors
	if(dbus_message_get_type(msg->msg) == DBUS_MESSAGE_TYPE_ERROR) {
		printf("Set Excited reply: %s.\n", dbus_message_get_error_name(msg->msg));
		stop = true;
		return;
	}

	printf("Excited value toggled.\n");
}

