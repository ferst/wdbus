/* HelloWorld3.c: A polite greeter.
 *
 * This example expands HelloWorld2.c with another of the HelloWorld interface
 * "br.wdbus.Example.HelloWorld3". We'll receive an argument with the name that
 * we should greet. The wdbus_message_get_arg_type is used to check if the
 * argument type is what we expected and wdbus_message_get_basic is used to get
 * the data.
 */
#include "wdbus.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>

/*Example interface Definition*/
// XML for this interface
#define HELLO_INTERFACE \
	"  <interface name=\"br.wdbus.Example.HelloWorld3\">"\
	"    <method name=\"Hello\">"\
	"      <arg name=\"name\" direction=\"in\" type=\"s\" />"\
	"      <arg name=\"resp\" direction=\"out\" type=\"s\" />"\
	"    </method>"\
	"  </interface>"

// Callback for the "Hello" method
DBusMessage *respond_to_hello(WDBusObject *object, DBusMessage *rqst)
{
	WDBusMessage request, reply;
	char resp[256], *buf;

	// Init. WDBusMessage to iterate over arguments
	if(wdbus_message_init(&request, rqst, WDBUS_MESSAGE_IN)) {
		wdbus_message_init(&reply, dbus_message_new_error(request.msg,
				DBUS_ERROR_INVALID_SIGNATURE, "This method requires at least one argument"), WDBUS_MESSAGE_OUT);
	} else {
		// Check first argument type
		if(wdbus_message_get_arg_type(&request) != DBUS_TYPE_STRING) {
			wdbus_message_init(&reply, dbus_message_new_error(request.msg,
					DBUS_ERROR_INVALID_ARGS, "Wrong argument type"), WDBUS_MESSAGE_OUT);
		} else {
			// Get argument
			wdbus_message_get_basic(&request, &buf);
			if(!buf) {
				wdbus_message_init(&reply, dbus_message_new_error(request.msg,
						DBUS_ERROR_FAILED, "Could not get argument"), WDBUS_MESSAGE_OUT);
			} else {
				// Prepare message
				snprintf(resp, sizeof(resp), "Hello, %.*s!\n", sizeof(resp)-10, buf);
				buf = resp;

				wdbus_message_init(&reply, dbus_message_new_method_return(request.msg),
						WDBUS_MESSAGE_OUT);
				wdbus_message_append(&reply, DBUS_TYPE_STRING, &buf);
			}
		}
	}

	return reply.msg;
}

// Interface struct. WDBusInterface has a Flexible Array Member (FAM), so it
// can only be static initialized or malloc'ed.
WDBusInterface hello = {
	.xml = HELLO_INTERFACE,
	.xml_len = strlen(HELLO_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("br.wdbus.Example.HelloWorld3"),
	.get = wdbus_properties_default_get,
	.get_all = wdbus_properties_default_get_all,
	.set = wdbus_properties_default_set,
	.nmethod = 1,
	.methods = {
		{.name = "Hello", .callback = respond_to_hello},
	}
};

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo)
{
	stop = true;
}

int main(void)
{
	WDBusContext *ctx;
	WDBusObject *obj;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect and ask for the name "br.wdbus.Example"
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, "br.wdbus.Example", 0)) {
		printf("wdbus_context_conenct fail.\n");
		return 1;
	}

	// Create one object
	obj = wdbus_object_create("/br/wdbus/Example");
	if(!obj) {
		printf("wdbus_object_create fail.\n");
		return 1;
	}

	// Register the introspectable interface
	if(wdbus_object_register_interface(obj, &wdbus_introspectable_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		return 1;
	}

	// Register the example interface
	if(wdbus_object_register_interface(obj, &hello)) {
		printf("wdbus_register_interface hello fail.\n");
		return 1;
	}

	// Register the object on our context
	if(wdbus_context_register_object(ctx, obj)) {
		printf("wdbus_context_register_object fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	printf("Signal caught. Exiting...");

	// This will clean the context allocations and registered objects
	wdbus_context_free(ctx);

	printf("OK.\n");

	return 0;
}
