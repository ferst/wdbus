/* HelloWorld1.c: A shy first example of wdbus.
 *
 * This is our first example, a shy hello world that only responds locally.
 * We'll create a WDBusContext and a single WDBusObject with the
 * "br.wdbus.Example.HelloWorld" interface, which has a single "Hello" method
 * with no arguments and no return.
 */
#include "wdbus.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>

/*Example interface Definition*/
// XML for this interface
#define HELLO_INTERFACE \
	"  <interface name=\"br.wdbus.Example.HelloWorld\">"\
	"    <method name=\"Hello\">"\
	"    </method>"\
	"  </interface>"

// Callback for the "Hello" method
DBusMessage *respond_to_hello(WDBusObject *object, DBusMessage *request)
{
	WDBusMessage reply;

	printf("Hello, world.\n");

	// Even without return arguments, we need to send a reply to say that the
	// method execution was ok.
	wdbus_message_init(&reply, dbus_message_new_method_return(request), WDBUS_MESSAGE_OUT);

	return reply.msg;
}

// Interface struct. WDBusInterface has a Flexible Array Member (FAM), so it
// can only be static initialized or malloc'ed.
WDBusInterface hello = {
	.xml = HELLO_INTERFACE,
	.xml_len = strlen(HELLO_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("br.wdbus.Example.HelloWorld"),
	// WDBus provides a default implementation for the properties interface
	// that only return "Unknown property" for set/get and an empty dict for
	// get_all
	.get = wdbus_properties_default_get,
	.get_all = wdbus_properties_default_get_all,
	.set = wdbus_properties_default_set,
	.nmethod = 1,
	.methods = {
		{.name = "Hello", .callback = respond_to_hello},
	}
};

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo)
{
	stop = true;
}

int main(void)
{
	WDBusContext *ctx;
	WDBusObject *obj;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect and ask for the name "br.wdbus.Example"
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, "br.wdbus.Example", 0)) {
		printf("wdbus_context_conenct fail.\n");
		return 1;
	}

	// Create one object
	obj = wdbus_object_create("/br/wdbus/Example");
	if(!obj) {
		printf("wdbus_object_create fail.\n");
		return 1;
	}

	// Register the introspectable interface
	if(wdbus_object_register_interface(obj, &wdbus_introspectable_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		return 1;
	}

	// Register the example interface
	if(wdbus_object_register_interface(obj, &hello)) {
		printf("wdbus_register_interface hello fail.\n");
		return 1;
	}

	// Register the object on our context
	if(wdbus_context_register_object(ctx, obj)) {
		printf("wdbus_context_register_object fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	printf("Signal caught. Exiting...");

	// This will clean the context allocations and registered objects
	wdbus_context_free(ctx);

	printf("OK.\n");

	return 0;
}
