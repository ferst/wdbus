/* HelloWorld8.c: Emitting signals.
 *
 * This final example shows how to emit signals. We'll change the mainloop to
 * add our own epoll that will monitors wdbus' epfd and stdin. When the user
 * inputs something on the terminal, we'll create a signal with this
 * data and emit it using wdbus_object_emit_signal in a new interface
 * that we'll add to the "Other" object.
 */
#include "wdbus.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <unistd.h>

/*Example interfaces Definitions*/
// XML for the HelloWrold interface
#define HELLO_INTERFACE \
	"  <interface name=\"br.wdbus.Example.HelloWorld5\">"\
	"    <property type=\"i\" name=\"NumberOfGreetings\" access=\"read\"/>"\
	"    <property type=\"i\" name=\"Excited\" access=\"readwrite\"/>"\
	"    <method name=\"Hello\">"\
	"      <arg name=\"name\" direction=\"in\" type=\"s\" />"\
	"      <arg name=\"resp\" direction=\"out\" type=\"s\" />"\
	"    </method>"\
	"  </interface>"

// Struct for Object data
struct hello_data {
	int NumberOfGreetings;
	bool excited;
};

// Callback for "get"
DBusMessage *hello_properties_get(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	WDBusMessage reply;
	WDBusContainer variant;
	struct hello_data *data = wdbus_object_get_user_data(object);
	dbus_bool_t bool_val; // DBus boolean is 4 bytes due to the wire protocol

	if(!strncmp(property, "NumberOfGreetings", strlen("NumberOfGreetings")+1)) {
		wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
				WDBUS_MESSAGE_OUT);
		wdbus_variant_open(&reply, &variant, DBUS_TYPE_INT32_AS_STRING);
		wdbus_message_append(&reply, DBUS_TYPE_INT32, &data->NumberOfGreetings);
		wdbus_container_close(&reply);
	} else if(!strncmp(property, "Excited", strlen("Excited")+1)) {
		bool_val = data->excited;
		wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
				WDBUS_MESSAGE_OUT);
		wdbus_variant_open(&reply, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);
		wdbus_message_append(&reply, DBUS_TYPE_BOOLEAN, &bool_val);
		wdbus_container_close(&reply);
	} else {
		wdbus_message_init(&reply, dbus_message_new_error(request->msg,
					DBUS_ERROR_UNKNOWN_PROPERTY, "Unknown property"), WDBUS_MESSAGE_OUT);
	}

	return reply.msg;
}

// Callback for "get_all"
DBusMessage *hello_properties_get_all(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	const char *property_name = "NumberOfGreetings";
	WDBusMessage reply;
	WDBusContainer dict, entry, variant;
	struct hello_data *data = wdbus_object_get_user_data(object);
	dbus_bool_t bool_val;

	// "property" is always NULL for get_all

	wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
			WDBUS_MESSAGE_OUT);
	wdbus_dict_open(&reply, &dict, DBUS_TYPE_STRING_AS_STRING,
			DBUS_TYPE_VARIANT_AS_STRING);

	// Append NumberOfGreetings
	wdbus_entry_open(&reply, &entry);
	wdbus_message_append(&reply, DBUS_TYPE_STRING, &property_name);
	wdbus_variant_open(&reply, &variant, DBUS_TYPE_INT32_AS_STRING);
	wdbus_message_append(&reply, DBUS_TYPE_INT32, &data->NumberOfGreetings);
	wdbus_container_close(&reply);
	wdbus_container_close(&reply);

	// Append Excited
	bool_val = data->excited;
	property_name = "Excited";
	wdbus_entry_open(&reply, &entry);
	wdbus_message_append(&reply, DBUS_TYPE_STRING, &property_name);
	wdbus_variant_open(&reply, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);
	wdbus_message_append(&reply, DBUS_TYPE_BOOLEAN, &bool_val);
	wdbus_container_close_all(&reply);

	return reply.msg;
}

// Callback for "set"
DBusMessage *hello_properties_set(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	WDBusMessage reply;
	struct hello_data *data = wdbus_object_get_user_data(object);
	dbus_bool_t bool_val;

	if(!strncmp(property, "NumberOfGreetings", strlen("NumberOfGreetings")+1)) {
		wdbus_message_init(&reply, dbus_message_new_error(request->msg,
					DBUS_ERROR_PROPERTY_READ_ONLY, "NumberOfGreetings is read-only."),
				WDBUS_MESSAGE_OUT);
	} else if(!strncmp(property, "Excited", strlen("Excited")+1)) {
		if(wdbus_message_get_arg_type(request) != DBUS_TYPE_BOOLEAN) {
			wdbus_message_init(&reply, dbus_message_new_error(request->msg,
						DBUS_ERROR_INVALID_ARGS, "Excited is boolean."),
					WDBUS_MESSAGE_OUT);
		} else {
			wdbus_message_get_basic(request, &bool_val);
			data->excited = bool_val;
			wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
					WDBUS_MESSAGE_OUT);
		}
	} else {
		wdbus_message_init(&reply, dbus_message_new_error(request->msg,
					DBUS_ERROR_UNKNOWN_PROPERTY, "Unknown property"), WDBUS_MESSAGE_OUT);
	}

	return reply.msg;
}

// Callback for the "Hello" method
DBusMessage *respond_to_hello(WDBusObject *object, DBusMessage *rqst)
{
	WDBusMessage request, reply;
	char resp[256], *buf;
	struct hello_data *data = wdbus_object_get_user_data(object);

	// Init. WDBusMessage to iterate over arguments
	if(wdbus_message_init(&request, rqst, WDBUS_MESSAGE_IN)) {
		wdbus_message_init(&reply, dbus_message_new_error(request.msg,
				DBUS_ERROR_INVALID_SIGNATURE, "This method requires at least one argument"), WDBUS_MESSAGE_OUT);
	} else {
		// Check first argument type
		if(wdbus_message_get_arg_type(&request) != DBUS_TYPE_STRING) {
			wdbus_message_init(&reply, dbus_message_new_error(request.msg,
					DBUS_ERROR_INVALID_ARGS, "Wrong argument type"), WDBUS_MESSAGE_OUT);
		} else {
			// Get argument
			wdbus_message_get_basic(&request, &buf);
			if(!buf) {
				wdbus_message_init(&reply, dbus_message_new_error(request.msg,
						DBUS_ERROR_FAILED, "Could not get argument"), WDBUS_MESSAGE_OUT);
			} else {
				// Prepare message
				snprintf(resp, sizeof(resp), "Hello, %.*s%c\n", sizeof(resp)-10,
						buf, data->excited?'!':'.');
				buf = resp;

				wdbus_message_init(&reply, dbus_message_new_method_return(request.msg),
						WDBUS_MESSAGE_OUT);
				wdbus_message_append(&reply, DBUS_TYPE_STRING, &buf);

				// Change property value
				data->NumberOfGreetings++;
			}
		}
	}

	return reply.msg;
}

// Interface struct. WDBusInterface has a Flexible Array Member (FAM), so it
// can only be static initialized or malloc'ed.
WDBusInterface hello = {
	.xml = HELLO_INTERFACE,
	.xml_len = strlen(HELLO_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("br.wdbus.Example.HelloWorld4"),
	.get = hello_properties_get,
	.get_all = hello_properties_get_all,
	.set = hello_properties_set,
	.nmethod = 1,
	.methods = {
		{.name = "Hello", .callback = respond_to_hello},
	}
};

// XML for the UserInput interface
#define USER_INPUT_INTERFACE \
	"  <interface name=\"br.wdbus.Example.UserInput\">"\
	"    <signal name=\"UserData\">"\
	"      <arg type=\"s\" name=\"data\" />"\
	"    </signal>"\
	"  </interface>"

WDBusInterface user_input = {
	.xml = USER_INPUT_INTERFACE,
	.xml_len = strlen(USER_INPUT_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("br.wdbus.Example.UserInput"),
	.get = wdbus_properties_default_get,
	.get_all = wdbus_properties_default_get_all,
	.set = wdbus_properties_default_set,
	.nmethod = 0,
	.methods = {},
};

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo)
{
	stop = true;
}

// Helper function to destroy our objects
void destroy_hello_world_object(void *data)
{
	free(data);
}

// Helper function to create and register our objects
WDBusObject *create_hello_world_object(WDBusContext *ctx, const char *path)
{
	WDBusObject *obj;
	struct hello_data *data;

	// Allocate object data
	data = (struct hello_data *)malloc(sizeof(struct hello_data));
	if(!data) {
		printf("malloc(hello_data) fail.\n");
		return NULL;
	}

	data->NumberOfGreetings = 0;
	data->excited = true;

	// Create object
	obj = wdbus_object_create(path);
	if(!obj) {
		printf("wdbus_object_create fail.\n");
		free(data);
		return NULL;
	}

	// Register the introspectable interface
	if(wdbus_object_register_interface(obj, &wdbus_introspectable_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		free(data);
		wdbus_object_free(obj);
		return NULL;
	}

	// Register the properties interface
	if(wdbus_object_register_interface(obj, &wdbus_properties_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		free(data);
		wdbus_object_free(obj);
		return NULL;
	}

	// Register the example interface
	if(wdbus_object_register_interface(obj, &hello)) {
		printf("wdbus_register_interface hello fail.\n");
		free(data);
		wdbus_object_free(obj);
		return NULL;
	}

	// Set object data and the callback to free hello_data
	wdbus_object_set_user_data(obj, data, destroy_hello_world_object);

	// Register the object on our context
	if(wdbus_context_register_object(ctx, obj)) {
		printf("wdbus_context_register_object fail.\n");
		wdbus_object_free(obj);
		return NULL;
	}

	return obj;
}

int main(void)
{
	WDBusContext *ctx;
	WDBusObject *obj;
	WDBusMessage msg;
	int epfd, n;
	struct epoll_event event = {.events = EPOLLIN};
	char buffer[256], *buf = buffer;

	// Create our epoll
	epfd = epoll_create1(0);
	if(epfd < 0) {
		printf("epoll_create1 fail.\n");
		return 1;
	}

	// Add stdin to out epoll
	event.data.fd = STDIN_FILENO;
	if(epoll_ctl(epfd, EPOLL_CTL_ADD, event.data.fd, &event)) {
		printf("epoll_ctl stdin fail.\n");
		return 1;
	}

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Add context epfd to our epoll
	event.data.fd = wdbus_context_get_epfd(ctx);
	if(epoll_ctl(epfd, EPOLL_CTL_ADD, event.data.fd, &event)) {
		printf("epoll_ctl context_epfd fail.\n");
		return 1;
	}

	// Connect and ask for the name "br.wdbus.Example"
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, "br.wdbus.Example", 0)) {
		printf("wdbus_context_conenct fail.\n");
		return 1;
	}

	// Create objects
	create_hello_world_object(ctx, "/br/wdbus/Example/One");
	obj = create_hello_world_object(ctx, "/br/wdbus/Example/Other");

	// Register the UserInput interface
	if(wdbus_object_register_interface(obj, &user_input)) {
		printf("wdbus_register_interface user_input fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		n = epoll_wait(epfd, &event, 1, -1);
		if(n < 0) {
			// You should handle errno here
			continue;
		}

		if(event.data.fd == STDIN_FILENO) {
			// Read user input
			n = read(STDIN_FILENO, buffer, sizeof(buffer)-1);
			if(n > 0) {
				// Prepare buffer
				buffer[n] = '\0';
				if(buffer[n-1] == '\n') {
					buffer[n-1] = '\0';
				}

				// Emit the signal
				wdbus_message_init(&msg, dbus_message_new_signal(
							wdbus_object_get_path(obj),
							"br.wdbus.Example.UserInput", "UserData"),
						WDBUS_MESSAGE_OUT);
				wdbus_message_append(&msg, DBUS_TYPE_STRING, &buf);
				wdbus_object_emit_signal(obj, &msg);
			}
		} else {
			// wdbus event
			wdbus_loop(ctx, 0);
		}
	}

	// Cleanup
	printf("Signal caught. Exiting...");

	// This will clean the context allocations and registered objects
	wdbus_context_free(ctx);

	printf("OK.\n");

	return 0;
}
