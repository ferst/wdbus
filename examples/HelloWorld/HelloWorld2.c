/* HelloWorld2.c: A better greeter.
 *
 * This example expands HelloWorld1.c with a new version of the HelloWorld
 * interface: "br.wdbus.Example.HelloWorld2". Now the method return a string,
 * that is appended with the wdbus_message_append function.
 */
#include "wdbus.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>

/*Example interface Definition*/
// XML for this interface
#define HELLO_INTERFACE \
	"  <interface name=\"br.wdbus.Example.HelloWorld2\">"\
	"    <method name=\"Hello\">"\
	"      <arg name=\"resp\" direction=\"out\" type=\"s\" />"\
	"    </method>"\
	"  </interface>"

// Callback for the "Hello" method
DBusMessage *respond_to_hello(WDBusObject *object, DBusMessage *request)
{
	WDBusMessage reply;
	char resp[] = "Hello, world!.\n";
	char *buf = resp;

	// Init WDBusMessage to append reply arguments
	wdbus_message_init(&reply, dbus_message_new_method_return(request), WDBUS_MESSAGE_OUT);

	// This would segfault: see dbus_message_append_args warning in DBus documentation
	//wdbus_message_append(&reply, DBUS_TYPE_STRING, &resp);
	wdbus_message_append(&reply, DBUS_TYPE_STRING, &buf);

	return reply.msg;
}

// Interface struct. WDBusInterface has a Flexible Array Member (FAM), so it
// can only be static initialized or malloc'ed.
WDBusInterface hello = {
	.xml = HELLO_INTERFACE,
	.xml_len = strlen(HELLO_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("br.wdbus.Example.HelloWorld2"),
	.get = wdbus_properties_default_get,
	.get_all = wdbus_properties_default_get_all,
	.set = wdbus_properties_default_set,
	.nmethod = 1,
	.methods = {
		{.name = "Hello", .callback = respond_to_hello},
	}
};

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo)
{
	stop = true;
}

int main(void)
{
	WDBusContext *ctx;
	WDBusObject *obj;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect and ask for the name "br.wdbus.Example"
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, "br.wdbus.Example", 0)) {
		printf("wdbus_context_conenct fail.\n");
		return 1;
	}

	// Create one object
	obj = wdbus_object_create("/br/wdbus/Example");
	if(!obj) {
		printf("wdbus_object_create fail.\n");
		return 1;
	}

	// Register the introspectable interface
	if(wdbus_object_register_interface(obj, &wdbus_introspectable_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		return 1;
	}

	// Register the example interface
	if(wdbus_object_register_interface(obj, &hello)) {
		printf("wdbus_register_interface hello fail.\n");
		return 1;
	}

	// Register the object on our context
	if(wdbus_context_register_object(ctx, obj)) {
		printf("wdbus_context_register_object fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	printf("Signal caught. Exiting...");

	// This will clean the context allocations and registered objects
	wdbus_context_free(ctx);

	printf("OK.\n");

	return 0;
}
