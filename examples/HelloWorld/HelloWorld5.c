/* HelloWorld5.c: read-write property.
 *
 * We'll expand the HelloWorld4.c with a read-write property "Excite", that
 * controls if the greeting string ends with "." or "!". The get/get_all
 * callbacks will be more complex now, and the callback for set will
 * be implemented.
 */
#include "wdbus.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>

/*Example interface Definition*/
// XML for this interface
#define HELLO_INTERFACE \
	"  <interface name=\"br.wdbus.Example.HelloWorld5\">"\
	"    <property type=\"i\" name=\"NumberOfGreetings\" access=\"read\"/>"\
	"    <property type=\"i\" name=\"Excited\" access=\"readwrite\"/>"\
	"    <method name=\"Hello\">"\
	"      <arg name=\"name\" direction=\"in\" type=\"s\" />"\
	"      <arg name=\"resp\" direction=\"out\" type=\"s\" />"\
	"    </method>"\
	"  </interface>"

// Global object data
int NumberOfGreetings = 0;
bool excited = true;

// Callback for "get"
DBusMessage *hello_properties_get(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	WDBusMessage reply;
	WDBusContainer variant;
	dbus_bool_t bool_val; // DBus boolean is 4 bytes due to the wire protocol

	if(!strncmp(property, "NumberOfGreetings", strlen("NumberOfGreetings")+1)) {
		wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
				WDBUS_MESSAGE_OUT);
		wdbus_variant_open(&reply, &variant, DBUS_TYPE_INT32_AS_STRING);
		wdbus_message_append(&reply, DBUS_TYPE_INT32, &NumberOfGreetings);
		wdbus_container_close(&reply);
	} else if(!strncmp(property, "Excited", strlen("Excited")+1)) {
		bool_val = excited;
		wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
				WDBUS_MESSAGE_OUT);
		wdbus_variant_open(&reply, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);
		wdbus_message_append(&reply, DBUS_TYPE_BOOLEAN, &bool_val);
		wdbus_container_close(&reply);
	} else {
		wdbus_message_init(&reply, dbus_message_new_error(request->msg,
					DBUS_ERROR_UNKNOWN_PROPERTY, "Unknown property"), WDBUS_MESSAGE_OUT);
	}

	return reply.msg;
}

// Callback for "get_all"
DBusMessage *hello_properties_get_all(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	const char *property_name = "NumberOfGreetings";
	WDBusMessage reply;
	WDBusContainer dict, entry, variant;
	dbus_bool_t bool_val;

	// "property" is always NULL for get_all

	wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
			WDBUS_MESSAGE_OUT);
	wdbus_dict_open(&reply, &dict, DBUS_TYPE_STRING_AS_STRING,
			DBUS_TYPE_VARIANT_AS_STRING);

	// Append NumberOfGreetings
	wdbus_entry_open(&reply, &entry);
	wdbus_message_append(&reply, DBUS_TYPE_STRING, &property_name);
	wdbus_variant_open(&reply, &variant, DBUS_TYPE_INT32_AS_STRING);
	wdbus_message_append(&reply, DBUS_TYPE_INT32, &NumberOfGreetings);
	wdbus_container_close(&reply);
	wdbus_container_close(&reply);

	// Append Excited
	bool_val = excited;
	property_name = "Excited";
	wdbus_entry_open(&reply, &entry);
	wdbus_message_append(&reply, DBUS_TYPE_STRING, &property_name);
	wdbus_variant_open(&reply, &variant, DBUS_TYPE_BOOLEAN_AS_STRING);
	wdbus_message_append(&reply, DBUS_TYPE_BOOLEAN, &bool_val);
	wdbus_container_close_all(&reply);

	return reply.msg;
}

// Callback for "set"
DBusMessage *hello_properties_set(WDBusObject *object, WDBusInterface *interface, const char *property, WDBusMessage *request)
{
	WDBusMessage reply;
	dbus_bool_t bool_val;

	if(!strncmp(property, "NumberOfGreetings", strlen("NumberOfGreetings")+1)) {
		wdbus_message_init(&reply, dbus_message_new_error(request->msg,
					DBUS_ERROR_PROPERTY_READ_ONLY, "NumberOfGreetings is read-only."),
				WDBUS_MESSAGE_OUT);
	} else if(!strncmp(property, "Excited", strlen("Excited")+1)) {
		if(wdbus_message_get_arg_type(request) != DBUS_TYPE_BOOLEAN) {
			wdbus_message_init(&reply, dbus_message_new_error(request->msg,
						DBUS_ERROR_INVALID_ARGS, "Excited is boolean."),
					WDBUS_MESSAGE_OUT);
		} else {
			wdbus_message_get_basic(request, &bool_val);
			excited = bool_val;
			wdbus_message_init(&reply, dbus_message_new_method_return(request->msg),
					WDBUS_MESSAGE_OUT);
		}
	} else {
		wdbus_message_init(&reply, dbus_message_new_error(request->msg,
					DBUS_ERROR_UNKNOWN_PROPERTY, "Unknown property"), WDBUS_MESSAGE_OUT);
	}

	return reply.msg;
}

// Callback for the "Hello" method
DBusMessage *respond_to_hello(WDBusObject *object, DBusMessage *rqst)
{
	WDBusMessage request, reply;
	char resp[256], *buf;

	// Init. WDBusMessage to iterate over arguments
	if(wdbus_message_init(&request, rqst, WDBUS_MESSAGE_IN)) {
		wdbus_message_init(&reply, dbus_message_new_error(request.msg,
				DBUS_ERROR_INVALID_SIGNATURE, "This method requires at least one argument"), WDBUS_MESSAGE_OUT);
	} else {
		// Check first argument type
		if(wdbus_message_get_arg_type(&request) != DBUS_TYPE_STRING) {
			wdbus_message_init(&reply, dbus_message_new_error(request.msg,
					DBUS_ERROR_INVALID_ARGS, "Wrong argument type"), WDBUS_MESSAGE_OUT);
		} else {
			// Get argument
			wdbus_message_get_basic(&request, &buf);
			if(!buf) {
				wdbus_message_init(&reply, dbus_message_new_error(request.msg,
						DBUS_ERROR_FAILED, "Could not get argument"), WDBUS_MESSAGE_OUT);
			} else {
				// Prepare message
				snprintf(resp, sizeof(resp), "Hello, %.*s%c\n", sizeof(resp)-10,
						buf, excited?'!':'.');
				buf = resp;

				wdbus_message_init(&reply, dbus_message_new_method_return(request.msg),
						WDBUS_MESSAGE_OUT);
				wdbus_message_append(&reply, DBUS_TYPE_STRING, &buf);

				// Change property value
				NumberOfGreetings++;
			}
		}
	}

	return reply.msg;
}

// Interface struct. WDBusInterface has a Flexible Array Member (FAM), so it
// can only be static initialized or malloc'ed.
WDBusInterface hello = {
	.xml = HELLO_INTERFACE,
	.xml_len = strlen(HELLO_INTERFACE),
	.name_offset = WDBUS_DEFAULT_INTERFACE_NAME_OFFSET,
	.name_len = strlen("br.wdbus.Example.HelloWorld5"),
	.get = hello_properties_get,
	.get_all = hello_properties_get_all,
	.set = hello_properties_set,
	.nmethod = 1,
	.methods = {
		{.name = "Hello", .callback = respond_to_hello},
	}
};

// Global flag and signal handler to catch SIGINT and exit gracefully
bool stop;

void signal_handler(int signo)
{
	stop = true;
}

int main(void)
{
	WDBusContext *ctx;
	WDBusObject *obj;

	// Create context with a queue for 20 events
	ctx = wdbus_context_create(20);
	if(!ctx) {
		printf("wdbus_context_create fail.\n");
		return 1;
	}

	// Connect and ask for the name "br.wdbus.Example"
	if(wdbus_context_connect(ctx, DBUS_BUS_SESSION, "br.wdbus.Example", 0)) {
		printf("wdbus_context_conenct fail.\n");
		return 1;
	}

	// Create one object
	obj = wdbus_object_create("/br/wdbus/Example");
	if(!obj) {
		printf("wdbus_object_create fail.\n");
		return 1;
	}

	// Register the introspectable interface
	if(wdbus_object_register_interface(obj, &wdbus_introspectable_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		return 1;
	}

	// Register the properties interface
	if(wdbus_object_register_interface(obj, &wdbus_properties_interface)) {
		printf("wdbus_register_interface introspectable fail.\n");
		return 1;
	}

	// Register the example interface
	if(wdbus_object_register_interface(obj, &hello)) {
		printf("wdbus_register_interface hello fail.\n");
		return 1;
	}

	// Register the object on our context
	if(wdbus_context_register_object(ctx, obj)) {
		printf("wdbus_context_register_object fail.\n");
		return 1;
	}

	// End context setup
	if(wdbus_context_setup(ctx)) {
		printf("wdbus_context_setup fail.\n");
		return 1;
	}

	// Setup SIGINT handler
	stop = false;
	signal(SIGINT, signal_handler);

	// Main loop
	while(!stop) {
		wdbus_loop(ctx, -1);
	}

	// Cleanup
	printf("Signal caught. Exiting...");

	// This will clean the context allocations and registered objects
	wdbus_context_free(ctx);

	printf("OK.\n");

	return 0;
}
